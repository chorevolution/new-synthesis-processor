package eu.chorevolution.synthesisprocessor.business.model;

import java.util.List;

public class Bpmn2ChoreographyModel {

	private String name;
	private byte[] bpmn2Content;
	private byte[] bpmn2XSD;
	
	private boolean isValid;
	private List<String> listErrors;
	
	public Bpmn2ChoreographyModel() {
		
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getBpmn2Content() {
		return bpmn2Content;
	}

	public void setBpmn2Content(byte[] bpmn2Content) {
		this.bpmn2Content = bpmn2Content;
	}

	public byte[] getBpmn2XSD() {
		return bpmn2XSD;
	}

	public void setBpmn2XSD(byte[] bpmn2xsd) {
		bpmn2XSD = bpmn2xsd;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public List<String> getListErrors() {
		return listErrors;
	}

	public void setListErrors(List<String> listErrors) {
		this.listErrors = listErrors;
	}
	
	
	
	
}
