package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.SecurityFilterGeneratorException;
import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.SecurityFilterGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.SecurityFilterGeneratorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorComponentType;
import eu.chorevolution.synthesisprocessor.business.SecurityFilterGenerator;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;
import eu.chorevolution.synthesisprocessor.business.model.SecurityFilterModel;
import eu.chorevolution.synthesisprocessor.util.SynthesisProcessorUtil;

@RestController
@RequestMapping("/rest")
public class SecurityFilterGeneratorController {
	
	@Autowired
	private SynthesisProcessorUtil synthesisProcessorUtil;
	
	@Autowired
	private SecurityFilterGenerator service;

    @RequestMapping(value = "/generateSecurityFilterProvider", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public SecurityFilterGeneratorResponse generateSecurityFilterProvider(@RequestBody SecurityFilterGeneratorRequest securityFilterGeneratorRequest) throws SecurityFilterGeneratorException, SynthesisProcessorBusinessException {

    	SecurityFilterModel sfModel = new SecurityFilterModel();
    	
    	Bpmn2ChoreographyModel chModel = new Bpmn2ChoreographyModel();
    	chModel.setName(securityFilterGeneratorRequest.getChoreographyName());
    	sfModel.setChoreographyModel(chModel);

    	sfModel.setName(securityFilterGeneratorRequest.getName());
    	sfModel.setConnectionAccountType(securityFilterGeneratorRequest.getConnectionAccountType());
    	sfModel.setUsernameConnectionAccount(securityFilterGeneratorRequest.getUsernameConnectionAccount());
    	sfModel.setPasswordConnectionAccount(securityFilterGeneratorRequest.getPasswordConnectionAccount());
    	sfModel.setServiceInventoryDomain(securityFilterGeneratorRequest.getServiceInventoryDomain());
    	sfModel.setSecurityDescriptionContent(securityFilterGeneratorRequest.getSecurityDescriptionContent());
    	sfModel.setSecurityRoles(securityFilterGeneratorRequest.getSecurityRoles());

    	
    	SecurityFilterModel generatedSfModel = service.generateSecurityFilterProvider(sfModel);
    	
		String location = synthesisProcessorUtil.createArtifactURI(
				generatedSfModel.getChoreographyModel().getName(), SynthesisProcessorComponentType.SF,
				generatedSfModel.getResponseName(), SynthesisProcessorUtil.getWarExtension(), generatedSfModel.getWarArtifact());
    	
		generatedSfModel.setLocation(location);
		
		return new SecurityFilterGeneratorResponse(generatedSfModel.getResponseName(), location);

    }
    
    
    @RequestMapping(value = "/generateSecurityFilterClient", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public SecurityFilterGeneratorResponse generateSecurityFilterClient(@RequestBody SecurityFilterGeneratorRequest securityFilterGeneratorRequest) throws SecurityFilterGeneratorException, SynthesisProcessorBusinessException {

    	SecurityFilterModel sfModel = new SecurityFilterModel();
    	
    	Bpmn2ChoreographyModel chModel = new Bpmn2ChoreographyModel();
    	chModel.setName(securityFilterGeneratorRequest.getChoreographyName());
    	sfModel.setChoreographyModel(chModel);
    	
    	sfModel.setName(securityFilterGeneratorRequest.getName());
    	sfModel.setCustomAuthJar(securityFilterGeneratorRequest.getCustomAuthJar());
    	sfModel.setServiceInventoryDomain(securityFilterGeneratorRequest.getServiceInventoryDomain());
    	sfModel.setSecurityRoles(securityFilterGeneratorRequest.getSecurityRoles());

    	
    	SecurityFilterModel generatedSfModel = service.generateSecurityFilterClient(sfModel);

		String location = synthesisProcessorUtil.createArtifactURI(
				generatedSfModel.getChoreographyModel().getName(), SynthesisProcessorComponentType.SF,
				generatedSfModel.getResponseName(), SynthesisProcessorUtil.getWarExtension(), generatedSfModel.getWarArtifact());
		
		generatedSfModel.setLocation(location);
		
		return new SecurityFilterGeneratorResponse(generatedSfModel.getResponseName(), location);

    	
    }
    
}
