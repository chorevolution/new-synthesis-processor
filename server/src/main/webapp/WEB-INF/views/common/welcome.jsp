<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><spring:message code="common.welcome"/>!</h3>
      </div>
    </div>
  </div>
</div>