package eu.chorevolution.synthesisprocessor.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Utente implements java.io.Serializable {
	
	private Long id;
	private String nome;
	private String cognome;
	private String email;
	private String username;
	private String password;
	private Date dataNascita;

	private Set<Ruolo> ruoli = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Ruolo> getRuoli() {
		return ruoli;
	}

	public void addRuolo(Ruolo ruolo) {
		this.ruoli.add(ruolo);
	}

	public void addRuoli(Set<Ruolo> ruoli) {
		this.ruoli.addAll(ruoli);
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	
}
