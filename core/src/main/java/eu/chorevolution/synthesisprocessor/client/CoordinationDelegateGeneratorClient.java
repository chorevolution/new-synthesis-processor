/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.client;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.api.CoordinationDelegateGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.CoordinationDelegateGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.CoordinationDelegateGeneratorResponse;
import eu.chorevolution.synthesisprocessor.util.ClientPathUtils;
import eu.chorevolution.synthesisprocessor.util.GeneralUtils;

public class CoordinationDelegateGeneratorClient {

	private RestTemplate restTemplate = new RestTemplate();
	private String host;
	
	public CoordinationDelegateGeneratorClient(String host) {
		this.host = host;
	}
	
	public CoordinationDelegateGeneratorResponse generateWSDLCoordinationDelegateProsumer(
			CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest)
			throws CoordinationDelegateGeneratorException {
		
        HttpEntity<CoordinationDelegateGeneratorRequest> entity = new HttpEntity<CoordinationDelegateGeneratorRequest>(coordinationDelegateGeneratorRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<CoordinationDelegateGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_WSDL_COORDINATION_DELEGATE_PROSUMER_PATH, 
            		HttpMethod.POST, entity, CoordinationDelegateGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new CoordinationDelegateGeneratorException("", e);
		}
	}


	public CoordinationDelegateGeneratorResponse generateWSDLCoordinationDelegateClient(
			CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest)
			throws CoordinationDelegateGeneratorException {
		
        HttpEntity<CoordinationDelegateGeneratorRequest> entity = new HttpEntity<CoordinationDelegateGeneratorRequest>(coordinationDelegateGeneratorRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<CoordinationDelegateGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_WSDL_COORDINATION_DELEGATE_CLIENT_PATH, 
            		HttpMethod.POST, entity, CoordinationDelegateGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new CoordinationDelegateGeneratorException("", e);
		}
	}


	public CoordinationDelegateGeneratorResponse generateCoordinationDelegateProsumer(
			CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest)
			throws CoordinationDelegateGeneratorException {
		
        HttpEntity<CoordinationDelegateGeneratorRequest> entity = new HttpEntity<CoordinationDelegateGeneratorRequest>(coordinationDelegateGeneratorRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<CoordinationDelegateGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_COORDINATION_DELEGATE_PROSUMER_PATH, 
            		HttpMethod.POST, entity, CoordinationDelegateGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new CoordinationDelegateGeneratorException("", e);
		}
	}


	public CoordinationDelegateGeneratorResponse generateCoordinationDelegateClient(
			CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest)
			throws CoordinationDelegateGeneratorException {
		
        HttpEntity<CoordinationDelegateGeneratorRequest> entity = new HttpEntity<CoordinationDelegateGeneratorRequest>(coordinationDelegateGeneratorRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<CoordinationDelegateGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_COORDINATION_DELEGATE_CLIENT_PATH, 
            		HttpMethod.POST, entity, CoordinationDelegateGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new CoordinationDelegateGeneratorException("", e);
		}
	}


	public CoordinationDelegateGeneratorResponse generateConsumer(
			CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest)
			throws CoordinationDelegateGeneratorException {
		
        HttpEntity<CoordinationDelegateGeneratorRequest> entity = new HttpEntity<CoordinationDelegateGeneratorRequest>(coordinationDelegateGeneratorRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<CoordinationDelegateGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_COORDINATION_DELEGATE_CONSUMER_PATH, 
            		HttpMethod.POST, entity, CoordinationDelegateGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new CoordinationDelegateGeneratorException("", e);
		}
	}


	
}