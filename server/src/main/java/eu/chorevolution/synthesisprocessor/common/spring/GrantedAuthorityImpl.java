package eu.chorevolution.synthesisprocessor.common.spring;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityImpl implements GrantedAuthority {
	
	private static final String ROLE_PREFIX = "ROLE_";

	private String role;

	public GrantedAuthorityImpl(String role) {
		super();
		this.role = role;
	}


	@Override
	public String getAuthority() {
		return ROLE_PREFIX + role;
	}

	@Override
	public String toString() {
		return "[autority=" + role + "]";
	}
}
