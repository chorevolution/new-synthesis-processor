package eu.chorevolution.synthesisprocessor.business.model;

import eu.chorevolution.synthesisprocessor.api.domain.BindingComponentProtocolType;
import eu.chorevolution.synthesisprocessor.api.domain.InterfaceDescriptionType;

public class BindingComponentModel {

	private String bindingComponentName;
	private BindingComponentProtocolType bindingComponentProtocolType;
	private Bpmn2ChoreographyModel choreographyModel;
	private byte[] interfaceDescriptionContent;
	private InterfaceDescriptionType interfaceDescriptionType;
	
	private String location;
	private byte[] artifact;
	private byte[] bcWsdl;
	
	public BindingComponentModel() {
		
	}

	public String getBindingComponentName() {
		return bindingComponentName;
	}

	public void setBindingComponentName(String bindingComponentName) {
		this.bindingComponentName = bindingComponentName;
	}

	public BindingComponentProtocolType getBindingComponentProtocolType() {
		return bindingComponentProtocolType;
	}

	public void setBindingComponentProtocolType(BindingComponentProtocolType bindingComponentProtocolType) {
		this.bindingComponentProtocolType = bindingComponentProtocolType;
	}

	public Bpmn2ChoreographyModel getChoreographyModel() {
		return choreographyModel;
	}

	public void setChoreographyModel(Bpmn2ChoreographyModel choreographyModel) {
		this.choreographyModel = choreographyModel;
	}

	public byte[] getInterfaceDescriptionContent() {
		return interfaceDescriptionContent;
	}

	public void setInterfaceDescriptionContent(byte[] interfaceDescriptionContent) {
		this.interfaceDescriptionContent = interfaceDescriptionContent;
	}

	public InterfaceDescriptionType getInterfaceDescriptionType() {
		return interfaceDescriptionType;
	}

	public void setInterfaceDescriptionType(InterfaceDescriptionType interfaceDescriptionType) {
		this.interfaceDescriptionType = interfaceDescriptionType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public byte[] getArtifact() {
		return artifact;
	}

	public void setArtifact(byte[] artifact) {
		this.artifact = artifact;
	}

	public byte[] getBcWsdl() {
		return bcWsdl;
	}

	public void setBcWsdl(byte[] bcWsdl) {
		this.bcWsdl = bcWsdl;
	}
	
	
}
