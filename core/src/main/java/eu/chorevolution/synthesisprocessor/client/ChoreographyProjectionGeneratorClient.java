/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.synthesisprocessor.api.ChoreographyProjectionGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyProjectionGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyProjectionGeneratorResponse;
import eu.chorevolution.synthesisprocessor.util.ClientPathUtils;
import eu.chorevolution.synthesisprocessor.util.GeneralUtils;

public class ChoreographyProjectionGeneratorClient {
	
	private RestTemplate restTemplate = new RestTemplate();
	private String host;
	
	public ChoreographyProjectionGeneratorClient(String host) {
		this.host = host;
	}

	public ChoreographyProjectionGeneratorResponse generateChoreographyProjection(
			ChoreographyProjectionGeneratorRequest choreographyProjectionGeneratorRequest)
			throws ChoreographyProjectionGeneratorException {

        HttpEntity<ChoreographyProjectionGeneratorRequest> entity = new HttpEntity<ChoreographyProjectionGeneratorRequest>(choreographyProjectionGeneratorRequest,
        			GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<ChoreographyProjectionGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_CHOREOGRAPHY_PROJECTION_PATH, 
            		HttpMethod.POST, entity, ChoreographyProjectionGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new ChoreographyProjectionGeneratorException("", e);
		}
		
		
	}

	

}
