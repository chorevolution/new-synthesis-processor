package eu.chorevolution.synthesisprocessor.business.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.business.BusinessException;
import eu.chorevolution.synthesisprocessor.business.UtenteService;
import eu.chorevolution.synthesisprocessor.domain.Docente;
import eu.chorevolution.synthesisprocessor.domain.Ruolo;
import eu.chorevolution.synthesisprocessor.domain.Studente;
import eu.chorevolution.synthesisprocessor.domain.Utente;

@Service
public class JDBCUtenteServiceImpl implements UtenteService {

	@Autowired
	private DataSource datasource;

	@Override
	public Utente autentica(String username) throws BusinessException {
		Utente utente = null;
		Ruolo ruolo = null;
		Connection con = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			con = datasource.getConnection();
			String sql = "select * from utenti u where u.username = ?";
			st = con.prepareStatement(sql);
			st.setString(1, username);
			rs = st.executeQuery();
			if (rs.next()) {
				int tipologia = rs.getInt("tipologia");
				switch (tipologia) {
				case 1:
					utente = new Docente();
					((Docente)utente).setMatricola(rs.getString("matricola"));
					ruolo = new Ruolo();
					ruolo.setId(-1L);
					ruolo.setNome("docente");
					utente.addRuolo(ruolo);
					break;
				case 2:
					utente = new Studente();
					((Studente)utente).setMatricola(rs.getString("matricola"));
					ruolo = new Ruolo();
					ruolo.setId(-1L);
					ruolo.setNome("studente");
					utente.addRuolo(ruolo);
					break;

				default:
					utente = new Utente();
					break;
				}
				utente.setId(rs.getLong("id_utente"));
				utente.setUsername(username);
				utente.setPassword(rs.getString("password"));
				utente.setCognome(rs.getString("cognome"));
				utente.setNome(rs.getString("nome"));
				utente.setEmail(rs.getString("email"));
				utente.setDataNascita(rs.getDate("data_nascita"));
				Set<Ruolo> ruoli = findRoles(utente.getId(), con);
				utente.addRuoli(ruoli);
			} else {
				throw new BusinessException();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return utente;
	}
	
	private Set<Ruolo> findRoles(Long idUtente, Connection con) throws BusinessException {
		PreparedStatement st = null;
		ResultSet rs = null;
		Set<Ruolo> result = new HashSet<Ruolo>();
		try {
			st = con.prepareStatement("select r.* from ruoli_utenti ur, ruoli r where ur.id_utente = ? and ur.id_ruolo = r.id_ruolo");
			st.setLong(1, idUtente);
			rs = st.executeQuery();
			while (rs.next()) {
				
				Ruolo ruolo = new Ruolo();
				ruolo.setId(rs.getLong("id_ruolo"));
				ruolo.setNome(rs.getString("nome"));
				ruolo.setDescrizione(rs.getString("descrizione"));
				result.add(ruolo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		return result;
	}

}
