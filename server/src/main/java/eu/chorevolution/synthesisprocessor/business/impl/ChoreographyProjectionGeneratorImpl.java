package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.ChoreographyProjectionGeneratorException;
import eu.chorevolution.synthesisprocessor.business.ChoreographyProjectionGenerator;
import eu.chorevolution.synthesisprocessor.business.model.ChoreographyProjectionModel;
import eu.chorevolution.transformations.generativeapproach.bpmn2choreographyprojector.Bpmn2ChoreographyProjector;
import eu.chorevolution.transformations.generativeapproach.bpmn2choreographyprojector.Bpmn2ChoreographyProjectorRequest;
import eu.chorevolution.transformations.generativeapproach.bpmn2choreographyprojector.Bpmn2ChoreographyProjectorResponse;

@Service
public class ChoreographyProjectionGeneratorImpl implements ChoreographyProjectionGenerator {

	
	@Override
	public ChoreographyProjectionModel generateChoreographyProjection(ChoreographyProjectionModel cpModel) throws ChoreographyProjectionGeneratorException {
		
		try {

			Bpmn2ChoreographyProjectorRequest request = new Bpmn2ChoreographyProjectorRequest(
					cpModel.getBpmn2Content(),
					cpModel.getParticipant());

			Bpmn2ChoreographyProjector bpmn2ChoreographyProjector = new Bpmn2ChoreographyProjector();
			Bpmn2ChoreographyProjectorResponse response = bpmn2ChoreographyProjector.project(request);
			
			cpModel.setProjectedBpmn2Content(response.getBpmn2Content());
			
			return cpModel;
			
		} catch (Exception e) {
			throw new ChoreographyProjectionGeneratorException(e);
		}
		
		
	}

	
	
}
