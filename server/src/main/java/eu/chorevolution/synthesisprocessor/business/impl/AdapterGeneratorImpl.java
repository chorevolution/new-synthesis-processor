package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.AdapterGeneratorException;
import eu.chorevolution.synthesisprocessor.business.AdapterGenerator;
import eu.chorevolution.synthesisprocessor.business.model.AdapterModel;
import eu.chorevolution.transformations.generativeapproach.adgenerator.impl.ADGeneratorImpl;
import eu.chorevolution.transformations.generativeapproach.adgenerator.model.Adapter;

@Service
public class AdapterGeneratorImpl implements AdapterGenerator {
	
	
	@Override
	public AdapterModel generateAdapter(AdapterModel adapterModel) throws AdapterGeneratorException {
		
		
	      try {
	    	  
	    	  Adapter adapter = new ADGeneratorImpl().generateAdapter(adapterModel.getAdapterName(), 
	    			  adapterModel.getAdapterModel(), adapterModel.getSourceWsdl());
	        
	          adapterModel.setGeneratedWsdl(adapter.getWsdl());
	          adapterModel.setWarArtifact(adapter.getArtifact());
	          
	          return adapterModel;
	          
	       } catch (Exception e) {
	          throw new AdapterGeneratorException(e);
	       }
		
		
	}

	
	
}
