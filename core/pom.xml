<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2015 The CHOReVOLUTION project
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>eu.chorevolution.synthesisprocessor</groupId>
		<artifactId>parent</artifactId>
		<version>2.2.0-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	
	<artifactId>core</artifactId>

	<name>CHOReVOLUTION Synthesis Processor - CHOReVOLUTION Synthesis Processor CORE</name>

	<description>CHOReVOLUTION Synthesis Processor</description>

	<packaging>jar</packaging>

	<prerequisites>
		<maven>3.0</maven>
	</prerequisites>

	<organization>
		<name>The CHOReVOLUTION project</name>
		<url>http://www.chorevolution.eu</url>
	</organization>

	<url>http://www.chorevolution.eu</url>

   <developers>
	   <developer>
	      <id>marco_autili</id>
	      <name>Marco Autili</name>
	      <email>marco.autili@univaq.it</email>
	      <url>http://www.di.univaq.it/marco.autili/</url>
	      <organization>University of LAquila</organization>
	      <organizationUrl>http://www.univaq.it/en/</organizationUrl>
	      <roles>
	         <role>architect</role>
	      </roles>
	      <timezone>Italy/Rome</timezone>
	   </developer>
	   <developer>
	      <id>massimo_tivoli</id>
	      <name>Massimo Tivoli</name>
	      <email>massimo.tivoli@di.univaq.it</email>
	      <url>http://massimotivoli.wixsite.com/home/</url>
	      <organization>University of LAquila</organization>
	      <organizationUrl>http://www.univaq.it/en/</organizationUrl>
	      <roles>
	         <role>architect</role>
	      </roles>
	      <timezone>Italy/Rome</timezone>
	   </developer>
	   <developer>
	      <id>amleto_disalle</id>
	      <name>Amleto Di Salle</name>
	      <email>amleto.disalle@univaq.it</email>
	      <url>http://www.amletodisalle.it/</url>
	      <organization>University of LAquila</organization>
	      <organizationUrl>http://www.univaq.it/en/</organizationUrl>
	      <roles>
	         <role>architect</role>
	      </roles>
	      <timezone>Italy/Rome</timezone>
	   </developer>
	   <developer>
		   <id>lorenzo_delauretis</id>
		   <name>Lorenzo De Lauretis</name>
		   <email>lorenzo.delauretis@live.it</email>
		   <url>http://www.lorenzodelauretis.it/</url>
		   <organization>University of LAquila</organization>
		   <organizationUrl>http://www.univaq.it/en/</organizationUrl>
		   <roles>
		      <role>developer</role>
		   </roles>
		   <timezone>Italy/Rome</timezone>
		</developer>
	</developers>
   
   <scm>
      <connection>scm:git:ssh://git@gitlab.ow2.org/chorevolution/synthesis-processor.git</connection>
      <developerConnection>scm:git:ssh://git@gitlab.ow2.org/chorevolution/synthesis-processor.git</developerConnection>
      <url>https://gitlab.ow2.org/chorevolution/synthesis-processor</url>
      <tag>HEAD</tag>
   </scm>
   
   <issueManagement>
      <system>jira</system>
      <url>https://jira.ow2.org/browse/CRV</url>
   </issueManagement>
   
   <licenses>
      <license>
         <name>The Apache Software License, Version 2.0</name>
         <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
         <distribution>repo</distribution>
      </license>
   </licenses>
   
   <distributionManagement>
      <repository>
         <id>ow2-nexus-releases</id>
         <name>OW2 Release Repository</name>
         <url>http://repository.ow2.org/nexus/service/local/staging/deploy/maven2/</url>
      </repository>
      <snapshotRepository>
         <id>ow2-nexus-snapshots</id>
         <name>OW2 Snapshots Repository</name>
         <url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
      </snapshotRepository>
   </distributionManagement>

	<properties>
		<spring.version>5.0.4.RELEASE</spring.version>
	
		<slf4j.version>1.7.12</slf4j.version>
		<slf4j-log4j12.version>1.7.12</slf4j-log4j12.version>
		<junit.version>4.12</junit.version>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

	</properties>

	<repositories>
		<repository>
			<id>ow2-nexus-snapshots</id>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-releases</id>
			<url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<dependencies>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.5</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j-log4j12.version}</version>
			<scope>runtime</scope>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>${maven.compiler.source}</source>
					<target>${maven.compiler.target}</target>
					<useIncrementalCompilation>false</useIncrementalCompilation>
					<showWarnings>true</showWarnings>
					<showDeprecation>true</showDeprecation>
					<compilerArgument>-Xlint:unchecked</compilerArgument>
				</configuration>
			</plugin>
			<!-- Disable default license check and enforce specific -->
			<plugin>
				<groupId>com.mycila.maven-license-plugin</groupId>
				<artifactId>maven-license-plugin</artifactId>
				<version>3.0</version>
				<inherited>true</inherited>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.rat</groupId>
				<artifactId>apache-rat-plugin</artifactId>
				<version>0.11</version>
				<configuration>
					<excludes>
						<exclude>**/rat.txt</exclude>
						<exclude>**/build-copy-javadoc-files.xml</exclude>
						<exclude>**/*.log</exclude>
						<exclude>**/output/**</exclude>
						<exclude>.git/**</exclude>
						<exclude>logs/**</exclude>
						<exclude>**/.*</exclude>
					</excludes>
				</configuration>
				<executions>
					<execution>
						<id>rat-check</id>
						<phase>verify</phase>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.7</version>
				<executions>
				   <execution>
				      <id>copy-artifact-legal-files</id>
				      <phase>process-resources</phase>
				      <goals>
				         <goal>copy-resources</goal>
				      </goals>
				      <configuration>
				         <outputDirectory>${project.build.directory}/classes/META-INF</outputDirectory>
				         <resources>
				            <resource>
				               <directory>${basedir}</directory>
				               <includes>
				                  <include>LICENSE</include>
				                  <include>NOTICE</include>
				               </includes>
				            </resource>
				         </resources>
				      </configuration>
				   </execution>
				   <execution>
				      <id>copy-javadoc-legal-files</id>
				      <phase>process-resources</phase>
				      <goals>
				         <goal>copy-resources</goal>
				      </goals>
				      <configuration>
				         <outputDirectory>${project.build.directory}/apidocs/META-INF</outputDirectory>
				         <resources>
				            <resource>
				               <directory>${basedir}</directory>
				               <includes>
				                  <include>LICENSE</include>
				                  <include>NOTICE</include>
				               </includes>
				            </resource>
				         </resources>
				      </configuration>
				   </execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.4</version>
				<configuration>
					<additionalparam>-Xdoclint:none</additionalparam>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>