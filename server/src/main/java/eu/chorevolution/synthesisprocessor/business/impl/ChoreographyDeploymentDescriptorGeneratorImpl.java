package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.ChoreographyDeploymentDescriptorGeneratorException;
import eu.chorevolution.synthesisprocessor.business.ChoreographyDeploymentDescriptorGenerator;
import eu.chorevolution.synthesisprocessor.business.model.ChoreographyDeploymentDescriptorModel;
import eu.chorevolution.transformations.generativeapproach.choreographyspecificationgenerator.ChoreographySpecificationGenerator;
import eu.chorevolution.transformations.generativeapproach.choreographyspecificationgenerator.ChoreographySpecificationGeneratorRequest;
import eu.chorevolution.transformations.generativeapproach.choreographyspecificationgenerator.ChoreographySpecificationGeneratorResponse;

@Service
public class ChoreographyDeploymentDescriptorGeneratorImpl implements ChoreographyDeploymentDescriptorGenerator {
		
	@Override
	public ChoreographyDeploymentDescriptorModel generateChoreographyDeploymentDescriptor(ChoreographyDeploymentDescriptorModel choreographyDeploymentDescriptorGeneratorRequest) throws ChoreographyDeploymentDescriptorGeneratorException {
		
		
		try {

			ChoreographySpecificationGeneratorRequest request = new ChoreographySpecificationGeneratorRequest(
					choreographyDeploymentDescriptorGeneratorRequest.getChoreographyArchitecture());

			ChoreographySpecificationGeneratorResponse response = new ChoreographySpecificationGenerator()
					.generate(request);

			choreographyDeploymentDescriptorGeneratorRequest.setChoreographySpecification(response.getChoreographySpecification());
			
			return choreographyDeploymentDescriptorGeneratorRequest;

		} catch (Exception e) {
			throw new ChoreographyDeploymentDescriptorGeneratorException(e);
		} 
		
	}

	
	
}
