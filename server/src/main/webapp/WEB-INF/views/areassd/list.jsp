<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script>
$(document).ready(function() {
	$('#areessd_table').dataTable({
		"columns":[
	                {"data":"id"},
	                {"data":"codice"},
	                {"data":"denominazione"},		                
	                { "data": "action",
	                    "searchable": false,
	                    "sortable": false,
	                    "defaultContent": "",
	                    "render": function ( data, type, row, meta ) {
	                    	return "<a href='${pageContext.request.contextPath}/areassd/update?id=" + row.id + "'><i class='fa fa-edit'></i></a>" + " | "+ 
	                       		   "<a href='${pageContext.request.contextPath}/areassd/delete?id=" + row.id + "'><i class='fa fa-remove'></a>";
	                     }
	                  }
        ],
        "ajax": {
		    "url": "${pageContext.request.contextPath}/areassd/findallpaginated",
		    "data": addsortparams		    
		},
	});
    
	
});
</script>
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title"><spring:message code="areessd.titolo"/></h3>
      </div>
      <div class="box-body">
        <p><a href="${pageContext.request.contextPath}/areassd/create" class="btn btn-app"><i class="fa fa-plus"></i><spring:message code="common.create"/></a></p>
        <table id="areessd_table" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th><spring:message code="common.id"/></th>
            <th><spring:message code="areassd.codice"/></th>
            <th><spring:message code="areassd.denominazione"/></th>
            <th><spring:message code="common.action"/></th>
          </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
          <tr>
            <th><spring:message code="common.id"/></th>
            <th><spring:message code="areassd.codice"/></th>
            <th><spring:message code="areassd.denominazione"/></th>
            <th><spring:message code="common.action"/></th>
          </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
