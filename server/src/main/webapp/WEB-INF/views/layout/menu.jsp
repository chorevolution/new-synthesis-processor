<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
     <!-- sidebar menu: : style can be found in sidebar.less -->
     <ul class="sidebar-menu" data-widget="tree">
		<security:authorize access="isAuthenticated()">
			<security:authorize access="hasRole('amministratore')">
				<li class="active treeview menu-open">
		         <a href="#">
		           <i class="fa fa-dashboard"></i><span><spring:message code="menu.ssd"/></span>
		           <span class="pull-right-container">
		             <i class="fa fa-angle-left pull-right"></i>
		           </span>
		         </a>
		         <ul class="treeview-menu">
		           <li><a href="${pageContext.request.contextPath}/areassd/list"><i class="fa fa-circle-o"></i><spring:message code="menu.areessd"/></a></li>
		           <li><a href="${pageContext.request.contextPath}/ssd/list"><i class="fa fa-circle-o"></i><spring:message code="menu.settoriscientificodisciplinari"/></a></li>
		         </ul>
		       </li>
			</security:authorize>
			<security:authorize access="hasRole('presidentecad')">
				<li class="active treeview menu-open">
		         <a href="#">
		           <i class="fa fa-dashboard"></i> <span>Presidente di CAD</span>
		           <span class="pull-right-container">
		             <i class="fa fa-angle-left pull-right"></i>
		           </span>
		         </a>
		         <ul class="treeview-menu">
		           <li><a href="index.html"><i class="fa fa-circle-o"></i> Presidente di CAD Dashboard v1</a></li>
		           <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Presidente di CAD Dashboard v2</a></li>
		         </ul>
		       </li>			
			</security:authorize>
			<security:authorize access="hasRole('segreteriadidattica')">
				<li class="active treeview menu-open">
		         <a href="#">
		           <i class="fa fa-dashboard"></i> <span>Segreteria Didattica</span>
		           <span class="pull-right-container">
		             <i class="fa fa-angle-left pull-right"></i>
		           </span>
		         </a>
		         <ul class="treeview-menu">
		           <li><a href="index.html"><i class="fa fa-circle-o"></i> Segreteria Didattica Dashboard v1</a></li>
		           <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Segreteria Didattica Dashboard v2</a></li>
		         </ul>
		       </li>			
			</security:authorize>
			<security:authorize access="hasRole('studente')">
				<li class="active treeview menu-open">
		         <a href="#">
		           <i class="fa fa-dashboard"></i> <span>Studente</span>
		           <span class="pull-right-container">
		             <i class="fa fa-angle-left pull-right"></i>
		           </span>
		         </a>
		         <ul class="treeview-menu">
		           <li><a href="index.html"><i class="fa fa-circle-o"></i> Studente Dashboard v1</a></li>
		           <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Studente Dashboard v2</a></li>
		         </ul>
		       </li>			
			</security:authorize>
		</security:authorize>
     </ul>
   </section>
   <!-- /.sidebar -->
 </aside>