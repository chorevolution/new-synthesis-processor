<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>eu.chorevolution.synthesisprocessor</groupId>
		<artifactId>parent</artifactId>
		<version>2.2.0-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	
	<artifactId>server</artifactId>

	<name>CHOReVOLUTION Synthesis Processor - CHOReVOLUTION Synthesis Processor SERVER</name>

	<description>CHOReVOLUTION Synthesis Processor</description>

	<packaging>war</packaging>

	<prerequisites>
		<maven>3.0</maven>
	</prerequisites>

	<organization>
		<name>The CHOReVOLUTION project</name>
		<url>http://www.chorevolution.eu</url>
	</organization>

	<url>http://www.chorevolution.eu</url>

   <developers>
	   <developer>
	      <id>marco_autili</id>
	      <name>Marco Autili</name>
	      <email>marco.autili@univaq.it</email>
	      <url>http://www.di.univaq.it/marco.autili/</url>
	      <organization>University of LAquila</organization>
	      <organizationUrl>http://www.univaq.it/en/</organizationUrl>
	      <roles>
	         <role>architect</role>
	      </roles>
	      <timezone>Italy/Rome</timezone>
	   </developer>
	   <developer>
	      <id>massimo_tivoli</id>
	      <name>Massimo Tivoli</name>
	      <email>massimo.tivoli@di.univaq.it</email>
	      <url>http://massimotivoli.wixsite.com/home/</url>
	      <organization>University of LAquila</organization>
	      <organizationUrl>http://www.univaq.it/en/</organizationUrl>
	      <roles>
	         <role>architect</role>
	      </roles>
	      <timezone>Italy/Rome</timezone>
	   </developer>
	   <developer>
	      <id>amleto_disalle</id>
	      <name>Amleto Di Salle</name>
	      <email>amleto.disalle@univaq.it</email>
	      <url>http://www.amletodisalle.it/</url>
	      <organization>University of LAquila</organization>
	      <organizationUrl>http://www.univaq.it/en/</organizationUrl>
	      <roles>
	         <role>architect</role>
	      </roles>
	      <timezone>Italy/Rome</timezone>
	   </developer>
	   <developer>
		   <id>lorenzo_delauretis</id>
		   <name>Lorenzo De Lauretis</name>
		   <email>lorenzo.delauretis@live.it</email>
		   <url>http://www.lorenzodelauretis.it/</url>
		   <organization>University of LAquila</organization>
		   <organizationUrl>http://www.univaq.it/en/</organizationUrl>
		   <roles>
		      <role>developer</role>
		   </roles>
		   <timezone>Italy/Rome</timezone>
		</developer>
	</developers>
   
   <scm>
      <connection>scm:git:ssh://git@gitlab.ow2.org/chorevolution/synthesis-processor.git</connection>
      <developerConnection>scm:git:ssh://git@gitlab.ow2.org/chorevolution/synthesis-processor.git</developerConnection>
      <url>https://gitlab.ow2.org/chorevolution/synthesis-processor</url>
      <tag>HEAD</tag>
   </scm>
   
   <issueManagement>
      <system>jira</system>
      <url>https://jira.ow2.org/browse/CRV</url>
   </issueManagement>
   
   <licenses>
      <license>
         <name>The Apache Software License, Version 2.0</name>
         <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
         <distribution>repo</distribution>
      </license>
   </licenses>
   
   <distributionManagement>
      <repository>
         <id>ow2-nexus-releases</id>
         <name>OW2 Release Repository</name>
         <url>http://repository.ow2.org/nexus/service/local/staging/deploy/maven2/</url>
      </repository>
      <snapshotRepository>
         <id>ow2-nexus-snapshots</id>
         <name>OW2 Snapshots Repository</name>
         <url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
      </snapshotRepository>
   </distributionManagement>
   
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<spring.version>5.0.4.RELEASE</spring.version>
		<spring-security.version>5.0.0.RELEASE</spring-security.version>
		<jackson-annotations.version>2.9.3</jackson-annotations.version>
		<jackson.version>2.9.3</jackson.version>
		<tiles.version>3.0.8</tiles.version>
		<jstl.version>1.2.5</jstl.version>
		<slf4j.version>1.7.6</slf4j.version>
		<log4j12.version>1.2.17</log4j12.version>
		<java.version>1.8</java.version>
	</properties>
	
	<repositories>
		<repository>
			<id>ow2-nexus-snapshots</id>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-releases</id>
			<url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>
	
	<dependencies>
	
		<dependency>
        	<groupId>eu.chorevolution.synthesisprocessor</groupId>
        	<artifactId>core</artifactId>
        	<version>2.2.0-SNAPSHOT</version>
    	</dependency>
	
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>adapter-generator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>
	
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>cd-generator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
			<exclusions>
				<exclusion>
					<groupId>com.sun.xml.bind</groupId>
					<artifactId>jaxb-impl</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<dependency>
			<groupId>eu.chorevolution.validations</groupId>
			<artifactId>bpmn2choreography-validator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>
		
		<dependency>
			<groupId>eu.chorevolution.transformations.sfgenerator</groupId>
			<artifactId>sf-generator</artifactId>
			<version>2.1.0</version>
			<exclusions>
				<exclusion>
					<groupId>ch.qos.logback</groupId>
					<artifactId>logback-classic</artifactId>
				</exclusion>
				<exclusion>
					<groupId>eu.chorevolution.modelingnotations</groupId>
					<artifactId>eu.chorevolution.modelingnotations.security</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>bc-generator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>
	
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>cdconsumerpart-generator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>
		
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>choreographyarchitecture-generator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>
	
		
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>bpmn2choreography-projector</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>		
	
		<dependency>
			<groupId>eu.chorevolution.transformations.generativeapproach</groupId>
			<artifactId>choreographyspecification-generator</artifactId>
			<version>2.1.1-SNAPSHOT</version>
		</dependency>
	
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>5.1.45</version>
		</dependency>		
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet.jsp</groupId>
			<artifactId>javax.servlet.jsp-api</artifactId>
			<version>2.3.1</version>
			<scope>provided</scope>
		</dependency>
		<!-- Spring framework -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<!-- Spring Security -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${spring-security.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${spring-security.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-taglibs</artifactId>
			<version>${spring-security.version}</version>
		</dependency>

		<!-- Jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>${jackson-annotations.version}</version>
		</dependency>


		<!--tiles-servlet and tiles-jsp -->
		<dependency>
			<groupId>org.apache.tiles</groupId>
			<artifactId>tiles-servlet</artifactId>
			<version>${tiles.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.tiles</groupId>
			<artifactId>tiles-jsp</artifactId>
			<version>${tiles.version}</version>
		</dependency>
		<!-- JSTL -->
		<dependency>
			<groupId>org.apache.taglibs</groupId>
			<artifactId>taglibs-standard-spec</artifactId>
			<version>${jstl.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.taglibs</groupId>
			<artifactId>taglibs-standard-impl</artifactId>
			<version>${jstl.version}</version>
		</dependency>


		<!-- Logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j.version}</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>${log4j12.version}</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<defaultGoal>install</defaultGoal>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<encoding>${project.build.sourceEncoding}</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.0.2</version>
				<configuration>
					<encoding>${project.build.sourceEncoding}</encoding>
				</configuration>
			</plugin>
		</plugins>
		<finalName>synthesisprocessor</finalName>
	</build>
</project>
