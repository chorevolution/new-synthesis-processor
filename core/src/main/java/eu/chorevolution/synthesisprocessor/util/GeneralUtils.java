/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

public class GeneralUtils {

	public static ClientHttpRequestFactory getClientHttpRequestFactory() {
	    int connectTimeout = 15000;
	    int readTimeout = 500000;
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
	    clientHttpRequestFactory.setConnectTimeout(connectTimeout);
	    clientHttpRequestFactory.setReadTimeout(readTimeout);
	    return clientHttpRequestFactory;
	}
	
	
	public static HttpHeaders getDefaultHttpHeaders() {
		
	       List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
	        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
	        acceptableMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);

	        HttpHeaders headers = new HttpHeaders();
	        headers.setAccept(acceptableMediaTypes);
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        
	        return headers;
		
	}
	
}
