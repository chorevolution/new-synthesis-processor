package eu.chorevolution.synthesisprocessor.business.model;

import java.util.List;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyArchitectureComponentData;


public class ChoreographyArchitectureModel {

	private byte[] bpmn2Content;
	private List<ChoreographyArchitectureComponentData> clientParticipants;
	private List<ChoreographyArchitectureComponentData> prosumerParticipants;
	private List<ChoreographyArchitectureComponentData> providerParticipants;
	
	private byte[] choreographyArchitecture;
	
	public ChoreographyArchitectureModel() {
		
	}

	public byte[] getBpmn2Content() {
		return bpmn2Content;
	}

	public void setBpmn2Content(byte[] bpmn2Content) {
		this.bpmn2Content = bpmn2Content;
	}

	public List<ChoreographyArchitectureComponentData> getClientParticipants() {
		return clientParticipants;
	}

	public void setClientParticipants(List<ChoreographyArchitectureComponentData> list) {
		this.clientParticipants = list;
	}

	public List<ChoreographyArchitectureComponentData> getProsumerParticipants() {
		return prosumerParticipants;
	}

	public void setProsumerParticipants(List<ChoreographyArchitectureComponentData> prosumerParticipants) {
		this.prosumerParticipants = prosumerParticipants;
	}

	public List<ChoreographyArchitectureComponentData> getProviderParticipants() {
		return providerParticipants;
	}

	public void setProviderParticipants(List<ChoreographyArchitectureComponentData> providerParticipants) {
		this.providerParticipants = providerParticipants;
	}

	public byte[] getChoreographyArchitecture() {
		return choreographyArchitecture;
	}

	public void setChoreographyArchitecture(byte[] choreographyArchitecture) {
		this.choreographyArchitecture = choreographyArchitecture;
	}
	
	
	
}
