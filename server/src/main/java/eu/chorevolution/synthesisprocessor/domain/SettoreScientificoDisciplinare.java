package eu.chorevolution.synthesisprocessor.domain;

public class SettoreScientificoDisciplinare implements java.io.Serializable {

	private Long id;
	private String codice;
	private String denominazione;
	private AreaSSD area;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDenominazione() {
		return denominazione;
	}

	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}

	public AreaSSD getArea() {
		return area;
	}

	public void setArea(AreaSSD area) {
		this.area = area;
	}

}
