<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<header class="main-header">
	<a href="${pageContext.request.contextPath}/common/welcome" class="logo"> 
		<!-- mini logo for sidebar mini 50x50 pixels --> 
		<span class="logo-mini"><b>My</b>U</span> 
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>My</b> Univaq</span>
	</a>

	<nav class="navbar navbar-static-top">
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"> <span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
					<img src="${pageContext.request.contextPath}/resources/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> 
						<span class="hidden-xs"><security:authentication property="principal.username"/></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header"><img src="${pageContext.request.contextPath}/resources/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
							<p><security:authentication property="principal.utente.nome"/> <security:authentication property="principal.utente.cognome"/></p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat"><spring:message code="common.profile"/></a>
							</div>
							<div class="pull-right">
								<a href="${pageContext.request.contextPath}/logout" class="btn btn-default btn-flat"><spring:message code="common.logout"/></a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>