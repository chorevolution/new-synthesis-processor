package eu.chorevolution.synthesisprocessor.business.impl;

public class ConversionUtility {
	public static String addPercentSuffix(String s) {
		if (s == null || "".equals(s)) {
			return "%";
		}
		return s + "%";
	}
}
