package eu.chorevolution.synthesisprocessor.business.model;

import java.util.List;

import eu.chorevolution.synthesisprocessor.api.domain.ConnectionAccountType;

public class SecurityFilterModel {

	private Bpmn2ChoreographyModel choreographyModel;
	private ConnectionAccountType connectionAccountType;
	private byte[] customAuthJar;
	private String name;
	private String passwordConnectionAccount;
	private byte[] securityDescriptionContent;
	private List<String> securityRoles;
	private String serviceInventoryDomain;
	private String usernameConnectionAccount;
	
	private String responseName;
	private String location;
	private byte[] warArtifact;
	
	public SecurityFilterModel() {
		
	}

	public Bpmn2ChoreographyModel getChoreographyModel() {
		return choreographyModel;
	}

	public void setChoreographyModel(Bpmn2ChoreographyModel choreographyModel) {
		this.choreographyModel = choreographyModel;
	}

	public ConnectionAccountType getConnectionAccountType() {
		return connectionAccountType;
	}

	public void setConnectionAccountType(ConnectionAccountType connectionAccountType) {
		this.connectionAccountType = connectionAccountType;
	}

	public byte[] getCustomAuthJar() {
		return customAuthJar;
	}

	public void setCustomAuthJar(byte[] customAuthJar) {
		this.customAuthJar = customAuthJar;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPasswordConnectionAccount() {
		return passwordConnectionAccount;
	}

	public void setPasswordConnectionAccount(String passwordConnectionAccount) {
		this.passwordConnectionAccount = passwordConnectionAccount;
	}

	public byte[] getSecurityDescriptionContent() {
		return securityDescriptionContent;
	}

	public void setSecurityDescriptionContent(byte[] securityDescriptionContent) {
		this.securityDescriptionContent = securityDescriptionContent;
	}

	public List<String> getSecurityRoles() {
		return securityRoles;
	}

	public void setSecurityRoles(List<String> securityRoles) {
		this.securityRoles = securityRoles;
	}

	public String getServiceInventoryDomain() {
		return serviceInventoryDomain;
	}

	public void setServiceInventoryDomain(String serviceInventoryDomain) {
		this.serviceInventoryDomain = serviceInventoryDomain;
	}

	public String getUsernameConnectionAccount() {
		return usernameConnectionAccount;
	}

	public void setUsernameConnectionAccount(String usernameConnectionAccount) {
		this.usernameConnectionAccount = usernameConnectionAccount;
	}

	public String getResponseName() {
		return responseName;
	}

	public void setResponseName(String responseName) {
		this.responseName = responseName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public byte[] getWarArtifact() {
		return warArtifact;
	}

	public void setWarArtifact(byte[] warArtifact) {
		this.warArtifact = warArtifact;
	}
	
	
	
}
