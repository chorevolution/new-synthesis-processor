package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.business.ChoreographyValidator;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;
import eu.chorevolution.validations.bpmn2choreographyvalidator.Bpmn2ChoreographyValidator;
import eu.chorevolution.validations.bpmn2choreographyvalidator.Bpmn2ChoreographyValidatorRequest;
import eu.chorevolution.validations.bpmn2choreographyvalidator.Bpmn2ChoreographyValidatorResponse;

@Service
public class ChoreographyValidatorImpl implements ChoreographyValidator{

	
	@Override
	public Bpmn2ChoreographyModel validateChoreography(Bpmn2ChoreographyModel choreographyModel) throws ChoreographyValidatorException {
		
		try {
			Bpmn2ChoreographyValidatorRequest bpmn2ChoreographyValidatorRequest = new Bpmn2ChoreographyValidatorRequest();
			bpmn2ChoreographyValidatorRequest.setBpmn2Content(choreographyModel.getBpmn2Content());
			bpmn2ChoreographyValidatorRequest.setBpmn2XSD(choreographyModel.getBpmn2XSD());

			Bpmn2ChoreographyValidatorResponse response = new Bpmn2ChoreographyValidator()
					.validate(bpmn2ChoreographyValidatorRequest);

			choreographyModel.setValid(response.isValid());
			choreographyModel.setListErrors(response.getErrors());
			
			return choreographyModel;
			
		} catch (Exception e) {
			throw new ChoreographyValidatorException(e);
		}
		
		
	}

	
	
}
