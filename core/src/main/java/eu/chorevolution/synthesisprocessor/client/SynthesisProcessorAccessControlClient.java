/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.client;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.synthesisprocessor.api.AccessControlException;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorLoginRequest;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorLoginResponse;

public class SynthesisProcessorAccessControlClient {

	private RestTemplate restTemplate = new RestTemplate();
	private String host;
	
	public SynthesisProcessorAccessControlClient(String host) {
		this.host = host;
	}
	
	public SynthesisProcessorLoginResponse executeLogin (
			SynthesisProcessorLoginRequest splr) throws AccessControlException {

		Map<String, SynthesisProcessorLoginRequest> params = new HashMap<String, SynthesisProcessorLoginRequest>();
		params.put("request", splr);
		
		try {
			return restTemplate.getForObject(host, SynthesisProcessorLoginResponse.class, params);
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new AccessControlException("", e);
		}
		
	}

}
