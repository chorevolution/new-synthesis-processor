package eu.chorevolution.synthesisprocessor.presentation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import eu.chorevolution.synthesisprocessor.common.spring.ValidationUtility;
import eu.chorevolution.synthesisprocessor.domain.SettoreScientificoDisciplinare;

@Component
public class SSDValidator  implements Validator {

	@Override
	public boolean supports(Class<?> klass) {
		return SettoreScientificoDisciplinare.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SettoreScientificoDisciplinare ssd = (SettoreScientificoDisciplinare) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "codice", "errors.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "denominazione", "errors.required");
		ValidationUtility.rejectIfMaxLength(errors, "codice", "errors.maxlength", ssd.getCodice(), 20);
		ValidationUtility.rejectIfMaxLength(errors, "denominazione", "errors.maxlength", ssd.getDenominazione(), 200);
	}

}


