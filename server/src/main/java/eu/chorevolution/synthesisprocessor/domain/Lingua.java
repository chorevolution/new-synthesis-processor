package eu.chorevolution.synthesisprocessor.domain;

public enum Lingua {

	ITA, ENG;
}
