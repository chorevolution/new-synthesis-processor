/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorResponse;
import eu.chorevolution.synthesisprocessor.util.ClientPathUtils;
import eu.chorevolution.synthesisprocessor.util.GeneralUtils;

public class ChoreographyValidatorClient {

	private RestTemplate restTemplate = new RestTemplate();
	private String host;
	
	public ChoreographyValidatorClient(String host) {
		this.host = host;
	}

	public ChoreographyValidatorResponse validateChoreography(
			ChoreographyValidatorRequest choreographyValidatorRequest)
			throws ChoreographyValidatorException {
		
        HttpEntity<ChoreographyValidatorRequest> entity = new HttpEntity<ChoreographyValidatorRequest>(choreographyValidatorRequest, GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<ChoreographyValidatorResponse> result = restTemplate.exchange(host + ClientPathUtils.VALIDATE_CHOREOGRAPHY_PATH, 
            		HttpMethod.POST, entity, ChoreographyValidatorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new ChoreographyValidatorException("", e);
		}
	}

}
