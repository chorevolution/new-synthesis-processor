package eu.chorevolution.synthesisprocessor.business.model;

public class ChoreographyDeploymentDescriptorModel {

	private byte[] choreographyArchitecture;
	private byte[] choreographySpecification;
	
	public ChoreographyDeploymentDescriptorModel() {
		
		
	}

	public byte[] getChoreographyArchitecture() {
		return choreographyArchitecture;
	}

	public void setChoreographyArchitecture(byte[] choreographyArchitecture) {
		this.choreographyArchitecture = choreographyArchitecture;
	}

	public byte[] getChoreographySpecification() {
		return choreographySpecification;
	}

	public void setChoreographySpecification(byte[] choreographySpecification) {
		this.choreographySpecification = choreographySpecification;
	}
	
	
}
