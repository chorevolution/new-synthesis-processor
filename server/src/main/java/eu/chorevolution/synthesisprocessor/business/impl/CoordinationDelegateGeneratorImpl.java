package eu.chorevolution.synthesisprocessor.business.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.CoordinationDelegateGeneratorException;
import eu.chorevolution.synthesisprocessor.business.CoordinationDelegateGenerator;
import eu.chorevolution.synthesisprocessor.business.model.CoordinationDelegateModel;
import eu.chorevolution.transformations.generativeapproach.cdconsumerpartgenerator.CDConsumerPartGenerator;
import eu.chorevolution.transformations.generativeapproach.cdconsumerpartgenerator.impl.CDConsumerPartGeneratorImpl;
import eu.chorevolution.transformations.generativeapproach.cdgenerator.CDGenerator;
import eu.chorevolution.transformations.generativeapproach.cdgenerator.impl.CDGeneratorImpl;
import eu.chorevolution.transformations.generativeapproach.cdgenerator.model.CD;
import eu.chorevolution.transformations.generativeapproach.cdgenerator.model.ProjectionData;
import eu.chorevolution.transformations.generativeapproach.cdgenerator.model.TaskCorrelationData;
import eu.chorevolution.transformations.generativeapproach.cdgenerator.model.WSDLData;

@Service
public class CoordinationDelegateGeneratorImpl implements CoordinationDelegateGenerator {

		
	@Override
	public CoordinationDelegateModel generateWSDLCoordinationDelegateProsumer(CoordinationDelegateModel cdModel) throws CoordinationDelegateGeneratorException {
		
		
		try {
			ProjectionData projectionData = new ProjectionData();
			projectionData.setCdName(cdModel.getName());
			projectionData.setParticipantName(cdModel.getProjectedParticipant());
			projectionData.setBpmn(cdModel.getProjectedBpmn2Content());
			projectionData.setTypes(cdModel.getProjectedTypesContent());

			CDGenerator cdGenerator = new CDGeneratorImpl();

			byte[] wsdl = cdGenerator.generateCDWSDL(projectionData);
			cdModel.setCdWsdl(wsdl);
			
			return cdModel;

		} catch (Exception e) {
			throw new CoordinationDelegateGeneratorException(e);
		}
		
		
	}

	
	@Override
	public CoordinationDelegateModel generateWSDLCoordinationDelegateClient(CoordinationDelegateModel cdModel) throws CoordinationDelegateGeneratorException {
		
		
		try {
			ProjectionData projectionData = new ProjectionData();
			projectionData.setCdName(cdModel.getName());
			projectionData.setParticipantName(cdModel.getProjectedParticipant());
			projectionData.setBpmn(cdModel.getProjectedBpmn2Content());
			projectionData.setTypes(cdModel.getProjectedTypesContent());

			
			List<TaskCorrelationData> taskCorrelationDatas = new ArrayList<TaskCorrelationData>();
			for (Map.Entry<String, String> entry : cdModel.getCorrelations().entrySet()) {
				taskCorrelationDatas.add(new TaskCorrelationData(entry.getKey(), entry.getValue()));
			}

			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] wsdl = cdGenerator.generateCDClientWSDL(projectionData, taskCorrelationDatas);

			cdModel.setCdClientWsdl(wsdl);
			
			return cdModel;
			
		} catch (Exception e) {
			throw new CoordinationDelegateGeneratorException(e);
		}
		
		
	}
	
	@Override
	public CoordinationDelegateModel generateCoordinationDelegateProsumer(CoordinationDelegateModel cdModel) throws CoordinationDelegateGeneratorException {
		
		
		try {
			ProjectionData projectionData = new ProjectionData();
			projectionData.setCdName(cdModel.getName());
			projectionData.setParticipantName(cdModel.getProjectedParticipant());
			projectionData.setBpmn(cdModel.getProjectedBpmn2Content());
			projectionData.setTypes(cdModel.getProjectedTypesContent());

			List<WSDLData> wsdlDatas = new ArrayList<WSDLData>();
			for (Map.Entry<String, byte[]> entry : cdModel.getWsdlParticipants().entrySet()) {
				wsdlDatas.add(new WSDLData(entry.getKey(), entry.getValue()));
			}

			CDGenerator cdGenerator = new CDGeneratorImpl();

			CD response = cdGenerator.generateCDProsumer(cdModel.getProsumerAndClientParticipants(),cdModel.getClientParticipants() ,projectionData, wsdlDatas);

			cdModel.setCdName(response.getName());
			cdModel.setCdProsumerWsdl(response.getProsumerwsdl());
			cdModel.setArtifact(response.getArtifact());
			
			return cdModel;
			
		} catch (Exception e) {
			throw new CoordinationDelegateGeneratorException(e);
		}
		
		
	}
	
	@Override
	public CoordinationDelegateModel generateCoordinationDelegateClient(CoordinationDelegateModel cdModel) throws CoordinationDelegateGeneratorException {
		
		try {
			ProjectionData projectionData = new ProjectionData();
			projectionData.setCdName(cdModel.getName());
			projectionData.setParticipantName(cdModel.getProjectedParticipant());
			projectionData.setBpmn(cdModel.getProjectedBpmn2Content());
			projectionData.setTypes(cdModel.getProjectedTypesContent());

			List<WSDLData> wsdlDatas = new ArrayList<WSDLData>();
			for (Map.Entry<String, byte[]> entry : cdModel.getWsdlParticipants()
					.entrySet()) {
				wsdlDatas.add(new WSDLData(entry.getKey(), entry.getValue()));
			}

			List<TaskCorrelationData> taskCorrelationDatas = new ArrayList<TaskCorrelationData>();
			for (Map.Entry<String, String> entry : cdModel.getCorrelations().entrySet()) {
				taskCorrelationDatas.add(new TaskCorrelationData(entry.getKey(), entry.getValue()));
			}

			CDGenerator cdGenerator = new CDGeneratorImpl();

			CD response = cdGenerator.generateCDClient(taskCorrelationDatas, projectionData, wsdlDatas);

			cdModel.setCdName(response.getName());
			cdModel.setArtifact(response.getArtifact());

			return cdModel;
			
		} catch (Exception e) {
			throw new CoordinationDelegateGeneratorException(e);
		}
		
		
	}
	
	@Override
	public CoordinationDelegateModel generateCoordinationDelegateConsumer(CoordinationDelegateModel cdModel) throws CoordinationDelegateGeneratorException {
		
		try {
			CDConsumerPartGenerator cdGenerator = new CDConsumerPartGeneratorImpl();

			byte[] artifact = cdGenerator.generateCDConsumerPart(cdModel.getName(),
					cdModel.getConsumerWsdlContent());

			cdModel.setConsumerArtifact(artifact);
			
			return cdModel;
			
		} catch (Exception e) {
			throw new CoordinationDelegateGeneratorException(e);
		}
		
		
	}
	
	
}
