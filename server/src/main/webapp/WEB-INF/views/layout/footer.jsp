<%@page import="java.util.Calendar"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<footer class="main-footer">
   <div class="pull-right hidden-xs">
     <b><spring:message code="common.version"/></b> 1.0.0
   </div>
   <strong><%=Calendar.getInstance().get(Calendar.YEAR)%> <a href="#"><spring:message code="common.applicationtitle"/></a>.</strong> <spring:message code="common.allrightreserved"/>.
</footer>
