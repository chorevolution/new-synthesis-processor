package eu.chorevolution.synthesisprocessor.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.chorevolution.synthesisprocessor.business.BusinessException;
import eu.chorevolution.synthesisprocessor.business.RequestGrid;
import eu.chorevolution.synthesisprocessor.business.ResponseGrid;
import eu.chorevolution.synthesisprocessor.business.SSDService;
import eu.chorevolution.synthesisprocessor.domain.AreaSSD;

@Controller
@RequestMapping("areassd")
public class AreaSSDController {
	
	@Autowired
	private SSDService service;
	
	@Autowired
	private AreaSSDValidator validator;
	
	@GetMapping("/list")
	public String list() {
		return "areassd.list";
	}
	
	@GetMapping("/findallpaginated")	
	public @ResponseBody ResponseGrid<AreaSSD> findAllPaginated(@ModelAttribute RequestGrid requestGrid) {
		return service.findAllAreeSSDPaginated(requestGrid);
	} 

	@GetMapping("/create")
	public String createStart(Model model) {
		AreaSSD areassd= new AreaSSD();
		model.addAttribute("areassd", areassd);
		return "areassd.create";
	}
	
	
	@PostMapping(value="/create")
	public String create(@ModelAttribute("areassd") AreaSSD areaSSD, BindingResult bindingResult) throws BusinessException {
		validator.validate(areaSSD, bindingResult);
		if (bindingResult.hasErrors()) {
			return "areassd.create";
		}

		service.createAreaSSD(areaSSD);
		return "redirect:/areassd/list";
	}
	
	@GetMapping("/update")
	public String updateStart(@RequestParam Long id, Model model) {
		AreaSSD areassd= service.findAreaSSDByID(id);
		model.addAttribute("areassd", areassd);
		return "areassd.update";
	}
	
	@PostMapping(value="/update")
	public String update(@ModelAttribute("areassd") AreaSSD areaSSD, BindingResult bindingResult) throws BusinessException {
		validator.validate(areaSSD, bindingResult);
		if (bindingResult.hasErrors()) {
			return "areassd.update";
		}

		service.updateAreaSSD(areaSSD);
		return "redirect:/areassd/list";
	}
	
	@GetMapping("/delete")
	public String deleteStart(@RequestParam Long id, Model model) {
		AreaSSD areassd= service.findAreaSSDByID(id);
		model.addAttribute("areassd", areassd);
		return "areassd.delete";
	}
	
	@PostMapping(value="/delete")
	public String delete(@ModelAttribute("areassd") AreaSSD areaSSD, BindingResult bindingResult) throws BusinessException {
		service.deleteAreaSSD(areaSSD);
		return "redirect:/areassd/list";
	}
}
