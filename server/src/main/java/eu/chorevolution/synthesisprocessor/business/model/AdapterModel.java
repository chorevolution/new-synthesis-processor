package eu.chorevolution.synthesisprocessor.business.model;

public class AdapterModel {

	private String adapterName;
	private byte[] adapterModel;
	private byte[] sourceWsdl;
	private byte[] generatedWsdl;
	private byte[] warArtifact;
	private Bpmn2ChoreographyModel choreographyModel;
	private String location;
	
	public AdapterModel() {
		
	}
	
	public String getAdapterName() {
		return adapterName;
	}
	public void setAdapterName(String adapterName) {
		this.adapterName = adapterName;
	}
	public byte[] getAdapterModel() {
		return adapterModel;
	}
	public void setAdapterModel(byte[] adapterModel) {
		this.adapterModel = adapterModel;
	}
	public byte[] getSourceWsdl() {
		return sourceWsdl;
	}
	public void setSourceWsdl(byte[] sourceWsdl) {
		this.sourceWsdl = sourceWsdl;
	}
	public byte[] getGeneratedWsdl() {
		return generatedWsdl;
	}
	public void setGeneratedWsdl(byte[] generatedWsdl) {
		this.generatedWsdl = generatedWsdl;
	}
	public byte[] getWarArtifact() {
		return warArtifact;
	}
	public void setWarArtifact(byte[] warArtifact) {
		this.warArtifact = warArtifact;
	}

	public Bpmn2ChoreographyModel getChoreographyModel() {
		return choreographyModel;
	}

	public void setChoreographyModel(Bpmn2ChoreographyModel choreographyModel) {
		this.choreographyModel = choreographyModel;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	
	
	
}
