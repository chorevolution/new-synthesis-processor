package eu.chorevolution.synthesisprocessor.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorComponentType;

public class SynthesisProcessorUtil {

	public SynthesisProcessorUtil() {
		
	}
	
	private static final String SEPARATOR_TIMESTAMP = "_";
	private static final String WAR_EXTENSION = ".war";
	private static final String TAR_GZ_EXTENSION = ".tar.gz";
	
	
	private String artifactHomeRepository = "./output";
	private String applicationBaseURI = "http://localhost:9091/synthesisprocessor/";
	
	public static String getHostUrl() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	    return request.getRequestURL().toString();
	}
	
	public static String getFullUrl() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	    return request.getRequestURL().toString() + "?" + request.getQueryString();
	}



	public String createArtifactURI(String choreographyName,
			SynthesisProcessorComponentType synthesisProcessorComponentType, String artifactName,
			String artifactExtension, byte[] artifactContent) throws SynthesisProcessorBusinessException {
		if (choreographyName == null || choreographyName.equals("")) {
			throw new SynthesisProcessorBusinessException("Choreography name is not specified");
		}

		try {
			String timestamp = getTimestamp();
			String fullArtifactName = artifactName + SEPARATOR_TIMESTAMP + timestamp + artifactExtension;

			File output = new File(artifactHomeRepository + File.separator + choreographyName + File.separator
					+ synthesisProcessorComponentType.getName() + File.separator + fullArtifactName);

			FileUtils.forceMkdir(output.getParentFile());
			FileUtils.writeByteArrayToFile(output, artifactContent);
			String baseUrl = applicationBaseURI;
			return baseUrl + choreographyName + "/" + synthesisProcessorComponentType.getName() + "/" + fullArtifactName;
		} catch (IOException e) {
			throw new SynthesisProcessorBusinessException("Internal Error while create artifact URI", e);
		}
	}

	private static String getTimestamp() {
		return new Date().getTime() + "";
	}

	public static String getWarExtension() {
		return WAR_EXTENSION;
	}

	public static String getTarGzExtension() {
		return TAR_GZ_EXTENSION;
	}
	
	
}
