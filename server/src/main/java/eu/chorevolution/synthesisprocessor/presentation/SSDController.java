package eu.chorevolution.synthesisprocessor.presentation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.chorevolution.synthesisprocessor.business.BusinessException;
import eu.chorevolution.synthesisprocessor.business.RequestGrid;
import eu.chorevolution.synthesisprocessor.business.ResponseGrid;
import eu.chorevolution.synthesisprocessor.business.SSDService;
import eu.chorevolution.synthesisprocessor.domain.AreaSSD;
import eu.chorevolution.synthesisprocessor.domain.SettoreScientificoDisciplinare;

@Controller
@RequestMapping("/ssd")
public class SSDController {

	@Autowired
	private SSDService service;

	@Autowired
	private SSDValidator validator;

	@GetMapping("/list")
	public String list() {
		return "ssd.list";
	}

	@GetMapping("/findallpaginated")

	public @ResponseBody ResponseGrid<SettoreScientificoDisciplinare> findAllPaginated(@ModelAttribute RequestGrid requestGrid) {
		return service.findAllSSDPaginated(requestGrid);
	}

	@GetMapping("/create")
	public String createStart(Model model) {
		SettoreScientificoDisciplinare ssd = new SettoreScientificoDisciplinare();
		ssd.setArea(new AreaSSD());
		model.addAttribute("ssd", ssd);
		return "ssd.create";
	}

	@PostMapping(value = "/create")
	public String create(@ModelAttribute("ssd") SettoreScientificoDisciplinare ssd, BindingResult bindingResult) throws BusinessException {
		validator.validate(ssd, bindingResult);
		if (bindingResult.hasErrors()) {
			return "ssd.create";
		}

		service.createSSD(ssd);
		return "redirect:/ssd/list";
	}

	@GetMapping("/update")
	public String updateStart(@RequestParam Long id, Model model) {
		SettoreScientificoDisciplinare ssd = service.findSSDByID(id);
		model.addAttribute("ssd", ssd);
		return "ssd.update";
	}

	@PostMapping(value = "/update")
	public String update(@ModelAttribute("ssd") SettoreScientificoDisciplinare ssd, BindingResult bindingResult) throws BusinessException {
		validator.validate(ssd, bindingResult);
		if (bindingResult.hasErrors()) {
			return "ssd.update";
		}

		service.updateSSD(ssd);
		return "redirect:/ssd/list";
	}

	@GetMapping("/delete")
	public String deleteStart(@RequestParam Long id, Model model) {
		SettoreScientificoDisciplinare ssd = service.findSSDByID(id);
		model.addAttribute("ssd", ssd);
		return "ssd.delete";
	}

	@PostMapping(value = "/delete")
	public String delete(@ModelAttribute("ssd") SettoreScientificoDisciplinare ssd, BindingResult bindingResult) throws BusinessException {
		service.deleteSSD(ssd);
		return "redirect:/ssd/list";
	}

	@ModelAttribute
	public void findAllAree(Model model) throws BusinessException {
		List<AreaSSD> areessd = service.findAllAree();
		model.addAttribute("areessd", areessd);
	}
}
