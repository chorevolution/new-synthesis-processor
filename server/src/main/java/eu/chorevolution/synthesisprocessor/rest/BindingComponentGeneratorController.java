package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.chorevolution.synthesisprocessor.api.BindingComponentGeneratorException;
import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.BindingComponentGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.BindingComponentGeneratorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorComponentType;
import eu.chorevolution.synthesisprocessor.business.BindingComponentGenerator;
import eu.chorevolution.synthesisprocessor.business.model.BindingComponentModel;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;
import eu.chorevolution.synthesisprocessor.util.SynthesisProcessorUtil;

@RestController
@RequestMapping("/rest")
public class BindingComponentGeneratorController {
	
	@Autowired
	private SynthesisProcessorUtil synthesisProcessorUtil;
	
	@Autowired
	private BindingComponentGenerator service;
	
    @RequestMapping(value = "/generateBindingComponent", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public BindingComponentGeneratorResponse generateBindingComponent(@RequestBody BindingComponentGeneratorRequest bindingComponentGeneratorRequest) throws BindingComponentGeneratorException, SynthesisProcessorBusinessException {
    	
    	BindingComponentModel bcModel = new BindingComponentModel();
    	bcModel.setBindingComponentName(bindingComponentGeneratorRequest.getBindingComponentName());
    	bcModel.setInterfaceDescriptionContent(bindingComponentGeneratorRequest.getInterfaceDescriptionContent());
    	bcModel.setBindingComponentProtocolType(bindingComponentGeneratorRequest.getBindingComponentProtocolType());
    	
    	Bpmn2ChoreographyModel chModel = new Bpmn2ChoreographyModel();
    	chModel.setName(bindingComponentGeneratorRequest.getChoreographyName());
    	bcModel.setChoreographyModel(chModel);
    	

		BindingComponentModel generatedBcModel = service.generateBindingComponent(bcModel);

		String location = synthesisProcessorUtil.createArtifactURI(
				generatedBcModel.getChoreographyModel().getName(), SynthesisProcessorComponentType.BC,
				generatedBcModel.getBindingComponentName(), SynthesisProcessorUtil.getWarExtension(), generatedBcModel.getArtifact());
		
		generatedBcModel.setLocation(location);
		
		return new BindingComponentGeneratorResponse(generatedBcModel.getArtifact(), generatedBcModel.getBcWsdl(), generatedBcModel.getLocation());

    	
    }
    
}
