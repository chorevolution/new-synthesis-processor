/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.util;

public class ClientPathUtils {

	//TODO remove these values and implement in the STUDIO as preferences!
	
	public static final String PREFIX_PATH = "/rest";
	
	public static final String DOWNLOAD_PATH = PREFIX_PATH + "/download";
	public static final String UPLOAD_PATH = PREFIX_PATH + "/upload";
	
	public static final String GENERATE_ADAPTER_PATH = PREFIX_PATH + "/generateAdapter";
	public static final String VALIDATE_CHOREOGRAPHY_PATH = PREFIX_PATH + "/validateChoreography";
	public static final String GENERATE_WSDL_COORDINATION_DELEGATE_PROSUMER_PATH = PREFIX_PATH + "/generateWSDLCoordinationDelegateProsumer";
	public static final String GENERATE_WSDL_COORDINATION_DELEGATE_CLIENT_PATH = PREFIX_PATH + "/generateWSDLCoordinationDelegateClient";
	public static final String GENERATE_COORDINATION_DELEGATE_PROSUMER_PATH = PREFIX_PATH + "/generateCoordinationDelegateProsumer";
	public static final String GENERATE_COORDINATION_DELEGATE_CLIENT_PATH = PREFIX_PATH + "/generateCoordinationDelegateClient";
	public static final String GENERATE_COORDINATION_DELEGATE_CONSUMER_PATH = PREFIX_PATH + "/generateCoordinationDelegateConsumer";
	public static final String GENERATE_CHOREOGRAPHY_PROJECTION_PATH = PREFIX_PATH + "/generateChoreographyProjection";
	public static final String GENERATE_BINDING_COMPONENT_PATH = PREFIX_PATH + "/generateBindingComponent";
	public static final String GENERATE_GIDL_FROM_WSDL_PATH = PREFIX_PATH + "/generateGidlFromWsdl";
	public static final String GENERATE_CHOREOGRAPHY_ARCHITECTURE_PATH = PREFIX_PATH + "/generateChoreographyArchitecture";
	public static final String GENERATE_CHOREOGRAPHY_DEPLOYMENT_DESCRIPTOR_PATH = PREFIX_PATH + "/generateChoreographyDeploymentDescriptor";
	public static final String GENERATE_SECURITY_FILTER_PROVIDER_PATH = PREFIX_PATH + "/generateSecurityFilterProvider";
	public static final String GENERATE_SECURITY_FILTER_CLIENT_PATH = PREFIX_PATH + "/generateSecurityFilterClient";


}
