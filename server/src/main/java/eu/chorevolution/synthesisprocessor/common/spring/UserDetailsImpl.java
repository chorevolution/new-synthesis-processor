package eu.chorevolution.synthesisprocessor.common.spring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import eu.chorevolution.synthesisprocessor.domain.Ruolo;
import eu.chorevolution.synthesisprocessor.domain.Utente;

public class UserDetailsImpl implements UserDetails {

	private Utente utente;

	public UserDetailsImpl(Utente utente) {
		super();
		this.utente = utente;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {

		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();
		for (Ruolo ruolo : utente.getRuoli()) {
			result.add(new GrantedAuthorityImpl(ruolo.getNome()));
			// result.add(new GrantedAuthorityImpl("ROLE_" + role.getName()));
		}
		return result;
	}

	@Override
	public String getPassword() {
		return utente.getPassword();
	}

	@Override
	public String getUsername() {
		return utente.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String toString() {
		return "UserDetailsImpl [username=" + utente.getUsername() + "]";
	}

	public Utente getUtente() {
		return utente;
	}
	

}
