package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.ChoreographyArchitectureGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyArchitectureComponentData;
import eu.chorevolution.synthesisprocessor.business.ChoreographyArchitectureGenerator;
import eu.chorevolution.synthesisprocessor.business.model.ChoreographyArchitectureModel;
import eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.model.AdapterComponentData;
import eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.model.BindingComponentData;
import eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.model.ComponentData;
import eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.model.ConsumerComponentData;
import eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.model.SecurityComponentData;

@Service
public class ChoreographyArchitectureGeneratorImpl implements ChoreographyArchitectureGenerator {
	

	@Override
	public ChoreographyArchitectureModel generateChoreographyArchitecture(ChoreographyArchitectureModel choreographyArchitectureModel) throws ChoreographyArchitectureGeneratorException {
		

		try {

			eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.ChoreographyArchitectureGeneratorRequest request = new eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.ChoreographyArchitectureGeneratorRequest();
			request.setBpmn2Content(choreographyArchitectureModel.getBpmn2Content());

			for (ChoreographyArchitectureComponentData choreographyArchitectureComponentData : choreographyArchitectureModel
					.getClientParticipants()) {
				ComponentData componentData = new ComponentData(
						choreographyArchitectureComponentData.getParticipantName(),
						choreographyArchitectureComponentData.getName(),
						choreographyArchitectureComponentData.getLocation());
				request.getClientParticipants().add(componentData);
				
				if (choreographyArchitectureComponentData.getBindingComponentData() != null) {
					BindingComponentData bindingComponentData = new BindingComponentData(
							choreographyArchitectureComponentData.getBindingComponentData().getName(),
							choreographyArchitectureComponentData.getBindingComponentData().getLocation());
					componentData.setBindingComponentData(bindingComponentData);
				}

				if (choreographyArchitectureComponentData.getConsumerComponentData() != null) {
					ConsumerComponentData consumerComponentData = new ConsumerComponentData(
							choreographyArchitectureComponentData.getConsumerComponentData().getName(),
							choreographyArchitectureComponentData.getConsumerComponentData().getLocation());
					componentData.setConsumerComponentData(consumerComponentData);
				
				}

				if (choreographyArchitectureComponentData.getSecurityComponentData() != null) {
					SecurityComponentData securityComponentData = new SecurityComponentData(
							choreographyArchitectureComponentData.getSecurityComponentData().getName(),
							choreographyArchitectureComponentData.getSecurityComponentData().getLocation());
					componentData.setSecurityComponentData(securityComponentData);
				}
				
				if (choreographyArchitectureComponentData.getAdapterComponentData() != null) {
					AdapterComponentData adapterComponentData = new AdapterComponentData(
							choreographyArchitectureComponentData.getAdapterComponentData().getName(),
							choreographyArchitectureComponentData.getAdapterComponentData().getLocation());
					componentData.setAdapterComponentData(adapterComponentData);
				}
			}

			for (ChoreographyArchitectureComponentData choreographyArchitectureComponentData : choreographyArchitectureModel
					.getProsumerParticipants()) {
				ComponentData componentData = new ComponentData(
						choreographyArchitectureComponentData.getParticipantName(),
						choreographyArchitectureComponentData.getName(),
						choreographyArchitectureComponentData.getLocation());
				request.getProsumerParticipants().add(componentData);
				
				if (choreographyArchitectureComponentData.getBindingComponentData() != null) {
					BindingComponentData bindingComponentData = new BindingComponentData(
							choreographyArchitectureComponentData.getBindingComponentData().getName(),
							choreographyArchitectureComponentData.getBindingComponentData().getLocation());
					componentData.setBindingComponentData(bindingComponentData);
				}

				if (choreographyArchitectureComponentData.getConsumerComponentData() != null) {
					ConsumerComponentData consumerComponentData = new ConsumerComponentData(
							choreographyArchitectureComponentData.getConsumerComponentData().getName(),
							choreographyArchitectureComponentData.getConsumerComponentData().getLocation());
					componentData.setConsumerComponentData(consumerComponentData);
				
				}

				if (choreographyArchitectureComponentData.getSecurityComponentData() != null) {
					SecurityComponentData securityComponentData = new SecurityComponentData(
							choreographyArchitectureComponentData.getSecurityComponentData().getName(),
							choreographyArchitectureComponentData.getSecurityComponentData().getLocation());
					componentData.setSecurityComponentData(securityComponentData);
				
				}
				
				if (choreographyArchitectureComponentData.getAdapterComponentData() != null) {
					AdapterComponentData adapterComponentData = new AdapterComponentData(
							choreographyArchitectureComponentData.getAdapterComponentData().getName(),
							choreographyArchitectureComponentData.getAdapterComponentData().getLocation());
					componentData.setAdapterComponentData(adapterComponentData);
				
				}
			}
			
			for (ChoreographyArchitectureComponentData choreographyArchitectureComponentData : choreographyArchitectureModel
					.getProviderParticipants()) {
				ComponentData componentData = new ComponentData(
						choreographyArchitectureComponentData.getParticipantName(),
						choreographyArchitectureComponentData.getName(),
						choreographyArchitectureComponentData.getLocation());
				request.getProviderParticipants().add(componentData);
				
				if (choreographyArchitectureComponentData.getBindingComponentData() != null) {
					BindingComponentData bindingComponentData = new BindingComponentData(
							choreographyArchitectureComponentData.getBindingComponentData().getName(),
							choreographyArchitectureComponentData.getBindingComponentData().getLocation());
					componentData.setBindingComponentData(bindingComponentData);
				}

				if (choreographyArchitectureComponentData.getConsumerComponentData() != null) {
					ConsumerComponentData consumerComponentData = new ConsumerComponentData(
							choreographyArchitectureComponentData.getConsumerComponentData().getName(),
							choreographyArchitectureComponentData.getConsumerComponentData().getLocation());
					componentData.setConsumerComponentData(consumerComponentData);
				
				}

				if (choreographyArchitectureComponentData.getSecurityComponentData() != null) {
					SecurityComponentData securityComponentData = new SecurityComponentData(
							choreographyArchitectureComponentData.getSecurityComponentData().getName(),
							choreographyArchitectureComponentData.getSecurityComponentData().getLocation());
					componentData.setSecurityComponentData(securityComponentData);
				
				}
				
				if (choreographyArchitectureComponentData.getAdapterComponentData() != null) {
					AdapterComponentData adapterComponentData = new AdapterComponentData(
							choreographyArchitectureComponentData.getAdapterComponentData().getName(),
							choreographyArchitectureComponentData.getAdapterComponentData().getLocation());
					componentData.setAdapterComponentData(adapterComponentData);
				
				}
			}
			
			eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.ChoreographyArchitectureGeneratorResponse response = new eu.chorevolution.transformations.generativeapproach.choreographyarchitecturegenerator.ChoreographyArchitectureGenerator().generate(request);

			choreographyArchitectureModel.setChoreographyArchitecture(response.getChoreographyArchitecture());
			return choreographyArchitectureModel;
			
		} catch (Exception e) {
			throw new ChoreographyArchitectureGeneratorException(e);
		}
		
		
		
	}

	
	
}
