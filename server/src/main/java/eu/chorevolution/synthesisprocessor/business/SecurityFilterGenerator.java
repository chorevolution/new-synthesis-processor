package eu.chorevolution.synthesisprocessor.business;

import eu.chorevolution.synthesisprocessor.api.SecurityFilterGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.SecurityFilterGeneratorResponse;
import eu.chorevolution.synthesisprocessor.business.model.SecurityFilterModel;

public interface SecurityFilterGenerator {

	SecurityFilterModel generateSecurityFilterProvider(SecurityFilterModel sfModel)
			throws SecurityFilterGeneratorException;

	SecurityFilterModel generateSecurityFilterClient(SecurityFilterModel sfModel)
			throws SecurityFilterGeneratorException;

}
