package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.AdapterGeneratorException;
import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.AdapterGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.AdapterGeneratorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorComponentType;
import eu.chorevolution.synthesisprocessor.business.AdapterGenerator;
import eu.chorevolution.synthesisprocessor.business.model.AdapterModel;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;
import eu.chorevolution.synthesisprocessor.util.SynthesisProcessorUtil;

@RestController
@RequestMapping("/rest")
public class AdapterGeneratorController {
	
	@Autowired
	private SynthesisProcessorUtil synthesisProcessorUtil;
	
	@Autowired
	private AdapterGenerator service;

    @RequestMapping(value = "/generateAdapter", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public AdapterGeneratorResponse generateAdapter(@RequestBody AdapterGeneratorRequest adapterGeneratorRequest) throws AdapterGeneratorException, SynthesisProcessorBusinessException {

    	AdapterModel adapterModel = new AdapterModel();
    	adapterModel.setAdapterName(adapterGeneratorRequest.getAdapterName());
    	adapterModel.setAdapterModel(adapterGeneratorRequest.getAdapterModel());
    	
    	Bpmn2ChoreographyModel choreographyModel = new Bpmn2ChoreographyModel();
    	choreographyModel.setBpmn2Content(adapterGeneratorRequest.getBpmn2Content());
    	choreographyModel.setBpmn2XSD(adapterGeneratorRequest.getTypesContent());
    	choreographyModel.setName(adapterGeneratorRequest.getChoreographyName());
    	
    	adapterModel.setChoreographyModel(choreographyModel);
    	adapterModel.setSourceWsdl(adapterGeneratorRequest.getWsdl());
    	
    	AdapterModel generatedAdapterModel = service.generateAdapter(adapterModel);

        String location = synthesisProcessorUtil.createArtifactURI(
        		generatedAdapterModel.getChoreographyModel().getName(), SynthesisProcessorComponentType.AD,
        		generatedAdapterModel.getAdapterName(), SynthesisProcessorUtil.getWarExtension(), generatedAdapterModel.getWarArtifact());
    	
        generatedAdapterModel.setLocation(location);
        
        return new AdapterGeneratorResponse(true, generatedAdapterModel.getGeneratedWsdl(), location);


    }
    
}
