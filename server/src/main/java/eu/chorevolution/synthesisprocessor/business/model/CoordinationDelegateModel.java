package eu.chorevolution.synthesisprocessor.business.model;

import java.util.List;
import java.util.Map;

public class CoordinationDelegateModel {
	
	private String name;
	private List<String> clientParticipants;
	private byte[] consumerWsdlContent;
	private Map<String,String> correlations;
	private Bpmn2ChoreographyModel choreographyModel;
	private byte[] projectedBpmn2Content;
	private byte[] projectedTypesContent;
	private String projectedParticipant;
	private List<String> prosumerAndClientParticipants;
	private Map<String,byte[]> wsdlParticipants;
	
	private byte[] cdWsdl;
	private byte[] cdClientWsdl;
	private byte[] cdProsumerWsdl;
	private byte[] consumerArtifact;
	private byte[] artifact;
	private String location;
	
	private String cdName;
	private String cdLocation;
	
	public CoordinationDelegateModel() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getClientParticipants() {
		return clientParticipants;
	}
	public void setClientParticipants(List<String> clientParticipants) {
		this.clientParticipants = clientParticipants;
	}
	public byte[] getConsumerWsdlContent() {
		return consumerWsdlContent;
	}
	public void setConsumerWsdlContent(byte[] consumerWsdlContent) {
		this.consumerWsdlContent = consumerWsdlContent;
	}
	public Map<String, String> getCorrelations() {
		return correlations;
	}
	public void setCorrelations(Map<String, String> correlations) {
		this.correlations = correlations;
	}
	public Bpmn2ChoreographyModel getChoreographyModel() {
		return choreographyModel;
	}
	public void setChoreographyModel(Bpmn2ChoreographyModel choreographyModel) {
		this.choreographyModel = choreographyModel;
	}
	public byte[] getProjectedBpmn2Content() {
		return projectedBpmn2Content;
	}
	public void setProjectedBpmn2Content(byte[] projectedBpmn2Content) {
		this.projectedBpmn2Content = projectedBpmn2Content;
	}
	public byte[] getProjectedTypesContent() {
		return projectedTypesContent;
	}
	public void setProjectedTypesContent(byte[] projectedTypesContent) {
		this.projectedTypesContent = projectedTypesContent;
	}
	public String getProjectedParticipant() {
		return projectedParticipant;
	}
	public void setProjectedParticipant(String projectedParticipant) {
		this.projectedParticipant = projectedParticipant;
	}
	public List<String> getProsumerAndClientParticipants() {
		return prosumerAndClientParticipants;
	}
	public void setProsumerAndClientParticipants(List<String> prosumerAndClientParticipants) {
		this.prosumerAndClientParticipants = prosumerAndClientParticipants;
	}
	public Map<String, byte[]> getWsdlParticipants() {
		return wsdlParticipants;
	}
	public void setWsdlParticipants(Map<String, byte[]> wsdlParticipants) {
		this.wsdlParticipants = wsdlParticipants;
	}
	public byte[] getCdWsdl() {
		return cdWsdl;
	}
	public void setCdWsdl(byte[] cdWsdl) {
		this.cdWsdl = cdWsdl;
	}

	public byte[] getCdClientWsdl() {
		return cdClientWsdl;
	}

	public void setCdClientWsdl(byte[] cdClientWsdl) {
		this.cdClientWsdl = cdClientWsdl;
	}

	public byte[] getCdProsumerWsdl() {
		return cdProsumerWsdl;
	}

	public void setCdProsumerWsdl(byte[] cdProsumerWsdl) {
		this.cdProsumerWsdl = cdProsumerWsdl;
	}

	public String getCdName() {
		return cdName;
	}

	public void setCdName(String cdName) {
		this.cdName = cdName;
	}

	public String getCdLocation() {
		return cdLocation;
	}

	public void setCdLocation(String cdLocation) {
		this.cdLocation = cdLocation;
	}

	public byte[] getConsumerArtifact() {
		return consumerArtifact;
	}

	public void setConsumerArtifact(byte[] consumerArtifact) {
		this.consumerArtifact = consumerArtifact;
	}

	public byte[] getArtifact() {
		return artifact;
	}

	public void setArtifact(byte[] artifact) {
		this.artifact = artifact;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
	
}
