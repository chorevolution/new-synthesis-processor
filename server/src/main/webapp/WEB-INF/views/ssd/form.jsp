<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
$(document).ready(function() {
	var del = "${requestScope.delete}"; 
	if (del == "true" ) {
		$(":input[type='text'],select[id='areassd_select2']").each(function () { $(this).attr('disabled','disabled'); });	
	}
	$('#areassd_select2').select2();
	
});
</script>

<div class="row">
<div class="col-xs-12">
<div class="box">
<div class="box-header">
  <h3 class="box-title"><spring:message code="ssd.titolo"/></h3>
</div>
<div class="box-body"> 
<form:form modelAttribute="ssd" action="${pageContext.request.contextPath}${requestScope.action}" cssClass="form-horizontal" method="POST">
	<form:hidden path="id"/>
	<div class="box-body">
	  <div class="form-group">
	    <label for="codice" class="col-sm-2 control-label"><spring:message code="ssd.codice"/></label>
	    <div class="col-sm-4">
			<form:input path="codice" cssClass="form-control"/>
   			<form:errors path="codice"/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="denominazione" class="col-sm-2 control-label"><spring:message code="ssd.denominazione"/></label>
	    <div class="col-sm-8">
			<form:input path="denominazione" cssClass="form-control"/>
   			<form:errors path="denominazione"/>
	    </div>
	  </div>	
	  <div class="form-group">
	    <label for="areassd" class="col-sm-2 control-label"><spring:message code="ssd.area"/></label>
	    <div class="col-sm-8">
			<form:select path="area.id" id="areassd_select2" cssClass="form-control">
    			<form:options items="${areessd}" itemLabel="denominazione" itemValue="id"/>
    		</form:select>   			
	    </div>
	  </div>		  
	  	
	</div>
	<div class="box-footer">
		<button type="submit" class="btn btn-info pull-left">
		  	<c:choose>
      		<c:when test="${!requestScope.delete}">
      			<spring:message code="common.submit"/>
      		</c:when>
      		<c:otherwise>
      			<spring:message code="common.delete"/>
      		</c:otherwise>
      		</c:choose>
		</button>
		<a href="${pageContext.request.contextPath}${requestScope.action_cancel}" class="btn btn-primary pull-right"><spring:message code="common.cancel"/></a>
	</div>
</form:form>    
</div>
</div>
</div>
</div>
