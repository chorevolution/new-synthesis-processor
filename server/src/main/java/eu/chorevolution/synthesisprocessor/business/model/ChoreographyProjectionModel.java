package eu.chorevolution.synthesisprocessor.business.model;

public class ChoreographyProjectionModel {

	private byte[] bpmn2Content;
	private byte[] projectedBpmn2Content;
	private String participant;
	
	public ChoreographyProjectionModel() {
		
	}

	public byte[] getBpmn2Content() {
		return bpmn2Content;
	}

	public void setBpmn2Content(byte[] bpmn2Content) {
		this.bpmn2Content = bpmn2Content;
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		this.participant = participant;
	}

	public byte[] getProjectedBpmn2Content() {
		return projectedBpmn2Content;
	}

	public void setProjectedBpmn2Content(byte[] projectedBpmn2Content) {
		this.projectedBpmn2Content = projectedBpmn2Content;
	}
	
}
