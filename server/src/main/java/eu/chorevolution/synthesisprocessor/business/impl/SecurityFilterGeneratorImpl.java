package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.SecurityFilterGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ConnectionAccountType;
import eu.chorevolution.synthesisprocessor.business.SecurityFilterGenerator;
import eu.chorevolution.synthesisprocessor.business.model.SecurityFilterModel;
import eu.chorevolution.transformations.sfgenerator.ConnectionAccount;
import eu.chorevolution.transformations.sfgenerator.LoginPasswordConnectionAccount;
import eu.chorevolution.transformations.sfgenerator.impl.SFGeneratorImpl;
import eu.chorevolution.transformations.sfgenerator.model.SF;

@Service
public class SecurityFilterGeneratorImpl implements SecurityFilterGenerator {
	
		
	@Override
	public SecurityFilterModel generateSecurityFilterProvider(SecurityFilterModel sfModel) throws SecurityFilterGeneratorException {
		
		try {
			ConnectionAccount connectionAccount = null;

			if (sfModel.getConnectionAccountType() != null && sfModel
					.getConnectionAccountType() == ConnectionAccountType.USERNAME_PASSWORD) {
				connectionAccount = new LoginPasswordConnectionAccount();
				((LoginPasswordConnectionAccount) connectionAccount)
						.setLogin(sfModel.getUsernameConnectionAccount());
				((LoginPasswordConnectionAccount) connectionAccount)
						.setPassword(sfModel.getPasswordConnectionAccount());
			}

			SF response = new SFGeneratorImpl().generateSecurityFilter(sfModel.getName(), "",
					sfModel.getServiceInventoryDomain(),
					sfModel.getSecurityDescriptionContent(),
					sfModel.getSecurityRoles(), connectionAccount);


			sfModel.setResponseName(response.getName());
			sfModel.setWarArtifact(response.getWar());
			
			return sfModel;

		} catch (Exception e) {
			throw new SecurityFilterGeneratorException(e);
		}
		
		
	}

	
	@Override
	public SecurityFilterModel generateSecurityFilterClient(SecurityFilterModel sfModel) throws SecurityFilterGeneratorException {
		
		try {
			
			SF response = null;
			
			//standard auth security filter
			if(sfModel.getCustomAuthJar() == null) {
				response = new SFGeneratorImpl().generateSecurityFilter(sfModel.getName(), "",
						sfModel.getServiceInventoryDomain(),
						sfModel.getSecurityRoles());
			}
			else {//custom jar security filter
				response = new SFGeneratorImpl().generateSecurityFilter(sfModel.getName(), "",
						sfModel.getServiceInventoryDomain(),
						sfModel.getSecurityRoles(),
						sfModel.getCustomAuthJar());
			}

			sfModel.setResponseName(response.getName());
			sfModel.setWarArtifact(response.getWar());
			
			return sfModel;

		} catch (Exception e) {
			throw new SecurityFilterGeneratorException(e);
		}
		
		
	}
	
	
}
