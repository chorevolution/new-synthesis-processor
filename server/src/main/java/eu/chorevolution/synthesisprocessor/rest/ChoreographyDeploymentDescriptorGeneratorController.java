package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.ChoreographyDeploymentDescriptorGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyDeploymentDescriptorGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyDeploymentDescriptorGeneratorResponse;
import eu.chorevolution.synthesisprocessor.business.ChoreographyDeploymentDescriptorGenerator;
import eu.chorevolution.synthesisprocessor.business.model.ChoreographyDeploymentDescriptorModel;

@RestController
@RequestMapping("/rest")
public class ChoreographyDeploymentDescriptorGeneratorController {
	
	@Autowired
	private ChoreographyDeploymentDescriptorGenerator service;

    @RequestMapping(value = "/generateChoreographyDeploymentDescriptor", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ChoreographyDeploymentDescriptorGeneratorResponse generateChoreographyDeploymentDescriptor(@RequestBody ChoreographyDeploymentDescriptorGeneratorRequest choreographyDeploymentDescriptorGeneratorRequest) throws ChoreographyDeploymentDescriptorGeneratorException {

    	ChoreographyDeploymentDescriptorModel cddModel = new ChoreographyDeploymentDescriptorModel();
    	cddModel.setChoreographyArchitecture(choreographyDeploymentDescriptorGeneratorRequest.getChoreographyArchitectureContent());
    	
    	ChoreographyDeploymentDescriptorModel generatedCddModel = service.generateChoreographyDeploymentDescriptor(cddModel);

		return new ChoreographyDeploymentDescriptorGeneratorResponse(generatedCddModel.getChoreographySpecification());

    }
    
}
