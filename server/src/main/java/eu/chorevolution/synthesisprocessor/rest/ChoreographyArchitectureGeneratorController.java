package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.ChoreographyArchitectureGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyArchitectureGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyArchitectureGeneratorResponse;
import eu.chorevolution.synthesisprocessor.business.ChoreographyArchitectureGenerator;
import eu.chorevolution.synthesisprocessor.business.model.ChoreographyArchitectureModel;

@RestController
@RequestMapping("/rest")
public class ChoreographyArchitectureGeneratorController {
	
	@Autowired
	private ChoreographyArchitectureGenerator service;

    @RequestMapping(value = "/generateChoreographyArchitecture", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ChoreographyArchitectureGeneratorResponse generateChoreographyArchitecture(@RequestBody ChoreographyArchitectureGeneratorRequest choreographyArchitectureGeneratorRequest) throws ChoreographyArchitectureGeneratorException {

    	ChoreographyArchitectureModel caModel = new ChoreographyArchitectureModel();
    	caModel.setBpmn2Content(choreographyArchitectureGeneratorRequest.getBpmn2Content());
    	caModel.setClientParticipants(choreographyArchitectureGeneratorRequest.getClientParticipants());
    	caModel.setProsumerParticipants(choreographyArchitectureGeneratorRequest.getProsumerParticipants());
    	caModel.setProviderParticipants(choreographyArchitectureGeneratorRequest.getProviderParticipants());

    	
    	ChoreographyArchitectureModel generatedCaModel = service.generateChoreographyArchitecture(caModel);

		return new ChoreographyArchitectureGeneratorResponse(generatedCaModel.getChoreographyArchitecture());

    	
    }
    
}
