package eu.chorevolution.synthesisprocessor.business.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.business.BusinessException;
import eu.chorevolution.synthesisprocessor.business.RequestGrid;
import eu.chorevolution.synthesisprocessor.business.ResponseGrid;
import eu.chorevolution.synthesisprocessor.business.SSDService;
import eu.chorevolution.synthesisprocessor.domain.AreaSSD;
import eu.chorevolution.synthesisprocessor.domain.SettoreScientificoDisciplinare;

@Service
public class JDBCSSDServiceImpl implements SSDService {

	@Autowired
	private DataSource dataSource;

	/* AREA SSD */
	@Override
	public ResponseGrid<AreaSSD> findAllAreeSSDPaginated(RequestGrid requestGrid) throws BusinessException {
		if ("id".equals(requestGrid.getSortCol())) {
			requestGrid.setSortCol("assd.id_area_ssd");
		}
		String orderBy = (!"".equals(requestGrid.getSortCol()) && !"".equals(requestGrid.getSortDir())) ? " ORDER BY " + requestGrid.getSortCol() + " " + requestGrid.getSortDir() : "";
		String baseSearch = "SELECT assd.* " + "FROM aree_ssd assd " + ((!"".equals(requestGrid.getSearch())) ? " WHERE assd.denominazione like '" + ConversionUtility.addPercentSuffix(requestGrid.getSearch()) + "'" : "");

		String sql = baseSearch + orderBy + " LIMIT " + requestGrid.getStart() + ", " + requestGrid.getLength();
		String countSql = "SELECT count(*) FROM (" + baseSearch + ") as t1";
		long records = 0;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<AreaSSD> ssds = new ArrayList<>();
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			// COUNT ELEMENTS
			rs = st.executeQuery(countSql);
			if (rs.next()) {
				records = rs.getLong(1);
			}
			// EXECUTE SQL LIMITED
			rs = st.executeQuery(sql);
			while (rs.next()) {
				AreaSSD ssd = new AreaSSD();
				ssd.setId(rs.getLong("id_area_ssd"));
				ssd.setCodice(rs.getString("codice"));
				ssd.setDenominazione(rs.getString("denominazione"));
				ssds.add(ssd);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return new ResponseGrid<AreaSSD>(requestGrid.getDraw(), records, records, ssds);
	}

	@Override
	public List<AreaSSD> findAllAree() throws BusinessException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<AreaSSD> result = new ArrayList<>();
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM aree_ssd ORDER BY codice");
			while (rs.next()) {
				AreaSSD areaSSD = new AreaSSD();
				areaSSD.setId(rs.getLong("id_area_ssd"));
				areaSSD.setCodice(rs.getString("codice"));
				areaSSD.setDenominazione(rs.getString("denominazione"));
				result.add(areaSSD);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return result;
	}

	@Override
	public AreaSSD findAreaSSDByID(Long id) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		AreaSSD result = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("SELECT assd.* FROM aree_ssd assd WHERE assd.id_area_ssd = ?");
			st.setLong(1, id);
			rs = st.executeQuery();
			if (rs.next()) {
				result = new AreaSSD();
				result.setId(rs.getLong("id_area_ssd"));
				result.setCodice(rs.getString("codice"));
				result.setDenominazione(rs.getString("denominazione"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return result;
	}

	@Override
	public void createAreaSSD(AreaSSD areaSSD) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("INSERT INTO aree_ssd (codice, denominazione) VALUES (?,?)");
			st.setString(1, areaSSD.getCodice());
			st.setString(2, areaSSD.getDenominazione());
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}

	}

	@Override
	public void updateAreaSSD(AreaSSD areaSSD) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("UPDATE aree_ssd SET codice=?, denominazione=? WHERE id_area_ssd=?");
			st.setString(1, areaSSD.getCodice());
			st.setString(2, areaSSD.getDenominazione());
			st.setLong(3, areaSSD.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}

	}

	@Override
	public void deleteAreaSSD(AreaSSD areaSSD) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("DELETE FROM aree_ssd WHERE id_area_ssd=?");
			st.setLong(1, areaSSD.getId());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}

	}

	/* Settore Scientifico Disciplinare */
	@Override
	public ResponseGrid<SettoreScientificoDisciplinare> findAllSSDPaginated(RequestGrid requestGrid) throws BusinessException {
		if ("id".equals(requestGrid.getSortCol())) {
			requestGrid.setSortCol("ssd.id_ssd");
		}
		String orderBy = (!"".equals(requestGrid.getSortCol()) && !"".equals(requestGrid.getSortDir())) ? " ORDER BY " + requestGrid.getSortCol() + " " + requestGrid.getSortDir() : "";
		String baseSearch = "SELECT ssd.*, areessd.codice as areessd_codice, areessd.denominazione as areessd_denominazione " + "FROM settori_scientifico_disciplinari ssd INNER JOIN aree_ssd areessd ON ssd.id_area_ssd=areessd.id_area_ssd" + ((!"".equals(requestGrid.getSearch())) ? " WHERE ssd.denominazione like '" + ConversionUtility.addPercentSuffix(requestGrid.getSearch()) + "'" : "");
		String sql = baseSearch + orderBy + " LIMIT " + requestGrid.getStart() + ", " + requestGrid.getLength();
		String countSql = "SELECT count(*) FROM (" + baseSearch + ") as t1";
		long records = 0;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<SettoreScientificoDisciplinare> ssds = new ArrayList<>();
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			// COUNT ELEMENTS
			rs = st.executeQuery(countSql);
			if (rs.next()) {
				records = rs.getLong(1);
			}
			// EXECUTE SQL LIMITED
			rs = st.executeQuery(sql);
			while (rs.next()) {
				SettoreScientificoDisciplinare ssd = new SettoreScientificoDisciplinare();
				ssd.setId(rs.getLong("id_ssd"));
				ssd.setCodice(rs.getString("codice"));
				ssd.setDenominazione(rs.getString("denominazione"));
				AreaSSD areassd = new AreaSSD();
				areassd.setId(rs.getLong("id_area_ssd"));
				areassd.setCodice(rs.getString("areessd_codice"));
				areassd.setDenominazione(rs.getString("areessd_denominazione"));
				ssd.setArea(areassd);
				ssds.add(ssd);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return new ResponseGrid<SettoreScientificoDisciplinare>(requestGrid.getDraw(), records, records, ssds);
	}

	@Override
	public SettoreScientificoDisciplinare findSSDByID(Long id) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		SettoreScientificoDisciplinare result = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("SELECT ssd.* FROM settori_scientifico_disciplinari ssd INNER JOIN aree_ssd areessd ON ssd.id_area_ssd=areessd.id_area_ssd WHERE ssd.id_ssd = ?");
			st.setLong(1, id);
			rs = st.executeQuery();
			if (rs.next()) {
				result = new SettoreScientificoDisciplinare();
				result.setId(rs.getLong("id_ssd"));
				result.setCodice(rs.getString("codice"));
				result.setDenominazione(rs.getString("denominazione"));
				AreaSSD areaSSD = new AreaSSD();
				areaSSD.setId(rs.getLong("id_area_ssd"));
				result.setArea(areaSSD);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return result;
	}

	@Override
	public void createSSD(SettoreScientificoDisciplinare ssd) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("INSERT INTO settori_scientifico_disciplinari (codice, denominazione, id_area_ssd) VALUES (?,?,?)");
			st.setString(1, ssd.getCodice());
			st.setString(2, ssd.getDenominazione());
			st.setLong(3, ssd.getArea().getId());
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}

	}

	@Override
	public void updateSSD(SettoreScientificoDisciplinare ssd) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("UPDATE settori_scientifico_disciplinari SET codice=?, denominazione=?, id_area_ssd=? WHERE id_ssd=?");
			st.setString(1, ssd.getCodice());
			st.setString(2, ssd.getDenominazione());
			st.setLong(3, ssd.getArea().getId());
			st.setLong(4, ssd.getId());

			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}

	}

	@Override
	public void deleteSSD(SettoreScientificoDisciplinare ssd) throws BusinessException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = dataSource.getConnection();
			st = con.prepareStatement("DELETE FROM settori_scientifico_disciplinari WHERE id_ssd=?");
			st.setLong(1, ssd.getId());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}

	}
}
