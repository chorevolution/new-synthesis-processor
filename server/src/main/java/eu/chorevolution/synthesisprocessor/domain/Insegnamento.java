package eu.chorevolution.synthesisprocessor.domain;

public class Insegnamento implements java.io.Serializable {

	private Long id;
	private String codice;
	private String denominazione;
	private int cfu;
	private TipologiaCredito tipologiaCredito;
	private Lingua lingua;
	private SettoreScientificoDisciplinare ssd;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDenominazione() {
		return denominazione;
	}

	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}

	public int getCfu() {
		return cfu;
	}

	public void setCfu(int cfu) {
		this.cfu = cfu;
	}

	public TipologiaCredito getTipologiaCredito() {
		return tipologiaCredito;
	}

	public void setTipologiaCredito(TipologiaCredito tipologiaCredito) {
		this.tipologiaCredito = tipologiaCredito;
	}

	public Lingua getLingua() {
		return lingua;
	}

	public void setLingua(Lingua lingua) {
		this.lingua = lingua;
	}

	public SettoreScientificoDisciplinare getSsd() {
		return ssd;
	}

	public void setSsd(SettoreScientificoDisciplinare ssd) {
		this.ssd = ssd;
	}

}
