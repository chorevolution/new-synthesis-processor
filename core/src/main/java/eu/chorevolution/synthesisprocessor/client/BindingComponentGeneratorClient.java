/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.synthesisprocessor.client;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.synthesisprocessor.api.BindingComponentGeneratorException;
import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.api.domain.BindingComponentGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.BindingComponentGeneratorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.GidlFromWSDLRequest;
import eu.chorevolution.synthesisprocessor.api.domain.GidlFromWSDLResponse;
import eu.chorevolution.synthesisprocessor.util.ClientPathUtils;
import eu.chorevolution.synthesisprocessor.util.GeneralUtils;

public class BindingComponentGeneratorClient {
	private RestTemplate restTemplate = new RestTemplate();
	private String host;

	public BindingComponentGeneratorClient(String host) {
		this.host = host;
	}

	public BindingComponentGeneratorResponse generateBindingComponent(
			BindingComponentGeneratorRequest bindingComponentGeneratorRequest)
			throws BindingComponentGeneratorException {
		
        HttpEntity<BindingComponentGeneratorRequest> entity = new HttpEntity<BindingComponentGeneratorRequest>(bindingComponentGeneratorRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<BindingComponentGeneratorResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_BINDING_COMPONENT_PATH, 
            		HttpMethod.POST, entity, BindingComponentGeneratorResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new BindingComponentGeneratorException("", e);
		}

	}

	public GidlFromWSDLResponse generateGIDLFromWSDL(GidlFromWSDLRequest gidlFromWSDLRequest)
			throws BindingComponentGeneratorException {

        HttpEntity<GidlFromWSDLRequest> entity = new HttpEntity<GidlFromWSDLRequest>(gidlFromWSDLRequest, 
        		GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<GidlFromWSDLResponse> result = restTemplate.exchange(host + ClientPathUtils.GENERATE_GIDL_FROM_WSDL_PATH, 
            		HttpMethod.POST, entity, GidlFromWSDLResponse.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new BindingComponentGeneratorException("", e);
		}

	}

}
