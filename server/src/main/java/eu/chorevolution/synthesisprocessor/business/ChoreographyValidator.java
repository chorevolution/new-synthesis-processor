package eu.chorevolution.synthesisprocessor.business;

import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorResponse;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;

public interface ChoreographyValidator {

	Bpmn2ChoreographyModel validateChoreography(Bpmn2ChoreographyModel choreographyModel)
			throws ChoreographyValidatorException;

	
	
}
