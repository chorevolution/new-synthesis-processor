package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.CoordinationDelegateGeneratorException;
import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.CoordinationDelegateGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.CoordinationDelegateGeneratorResponse;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorComponentType;
import eu.chorevolution.synthesisprocessor.business.CoordinationDelegateGenerator;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;
import eu.chorevolution.synthesisprocessor.business.model.CoordinationDelegateModel;
import eu.chorevolution.synthesisprocessor.util.SynthesisProcessorUtil;

@RestController
@RequestMapping("/rest")
public class CoordinationDelegateGeneratorController {
	
	@Autowired
	private SynthesisProcessorUtil synthesisProcessorUtil;
	
	@Autowired
	private CoordinationDelegateGenerator service;
	
    @RequestMapping(value = "/generateWSDLCoordinationDelegateProsumer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public CoordinationDelegateGeneratorResponse generateWSDLCoordinationDelegateProsumer(@RequestBody CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest) throws CoordinationDelegateGeneratorException {
	
    	
    	CoordinationDelegateModel cdModel = new CoordinationDelegateModel();
    	cdModel.setName(coordinationDelegateGeneratorRequest.getName());
    	cdModel.setProjectedParticipant(coordinationDelegateGeneratorRequest.getProjectedParticipant());
    	cdModel.setProjectedBpmn2Content(coordinationDelegateGeneratorRequest.getProjectedBpmn2Content());
    	cdModel.setProjectedTypesContent(coordinationDelegateGeneratorRequest.getProjectedTypesContent());
    	
    	CoordinationDelegateModel generatedCdModel = service.generateWSDLCoordinationDelegateProsumer(cdModel);
    	
		return new CoordinationDelegateGeneratorResponse(generatedCdModel.getProjectedParticipant(), generatedCdModel.getCdWsdl());
    	
    }
    
    
    @RequestMapping(value = "/generateWSDLCoordinationDelegateClient", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public CoordinationDelegateGeneratorResponse generateWSDLCoordinationDelegateClient(@RequestBody CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest) throws CoordinationDelegateGeneratorException {
	
    	CoordinationDelegateModel cdModel = new CoordinationDelegateModel();
    	cdModel.setName(coordinationDelegateGeneratorRequest.getName());
    	cdModel.setProjectedParticipant(coordinationDelegateGeneratorRequest.getProjectedParticipant());
    	cdModel.setProjectedBpmn2Content(coordinationDelegateGeneratorRequest.getProjectedBpmn2Content());
    	cdModel.setProjectedTypesContent(coordinationDelegateGeneratorRequest.getProjectedTypesContent());
		
    	cdModel.setCorrelations(coordinationDelegateGeneratorRequest.getCorrelations());
    	
    	CoordinationDelegateModel generatedCdModel = service.generateWSDLCoordinationDelegateClient(cdModel);

		return new CoordinationDelegateGeneratorResponse(generatedCdModel.getProjectedParticipant(), generatedCdModel.getCdClientWsdl());

    	
    }
    
    @RequestMapping(value = "/generateCoordinationDelegateProsumer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public CoordinationDelegateGeneratorResponse generateCoordinationDelegateProsumer(@RequestBody CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest) throws CoordinationDelegateGeneratorException, SynthesisProcessorBusinessException {
	
    	CoordinationDelegateModel cdModel = new CoordinationDelegateModel();
    	cdModel.setName(coordinationDelegateGeneratorRequest.getName());
    	cdModel.setProjectedParticipant(coordinationDelegateGeneratorRequest.getProjectedParticipant());
    	cdModel.setProjectedBpmn2Content(coordinationDelegateGeneratorRequest.getProjectedBpmn2Content());
    	cdModel.setProjectedTypesContent(coordinationDelegateGeneratorRequest.getProjectedTypesContent());
    	
    	cdModel.setWsdlParticipants(coordinationDelegateGeneratorRequest.getWsdlParticipants());
    	cdModel.setClientParticipants(coordinationDelegateGeneratorRequest.getClientParticipants());
    	cdModel.setProsumerAndClientParticipants(coordinationDelegateGeneratorRequest.getProsumerAndClientParticipants());

    	Bpmn2ChoreographyModel chorModel = new Bpmn2ChoreographyModel();
    	chorModel.setName(coordinationDelegateGeneratorRequest.getChoreographyName());
    	cdModel.setChoreographyModel(chorModel);
    	
    	CoordinationDelegateModel generatedCdModel = service.generateCoordinationDelegateProsumer(cdModel);

		String location = synthesisProcessorUtil.createArtifactURI(
				generatedCdModel.getChoreographyModel().getName(), SynthesisProcessorComponentType.CD,
				generatedCdModel.getCdName(), SynthesisProcessorUtil.getTarGzExtension(), generatedCdModel.getArtifact());
    	
		generatedCdModel.setLocation(location);
		
		return new CoordinationDelegateGeneratorResponse(generatedCdModel.getCdName(), generatedCdModel.getLocation(), generatedCdModel.getCdProsumerWsdl(), null);
    	
    }
    
    @RequestMapping(value = "/generateCoordinationDelegateClient", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public CoordinationDelegateGeneratorResponse generateCoordinationDelegateClient(@RequestBody CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest) throws CoordinationDelegateGeneratorException, SynthesisProcessorBusinessException {
	
    	CoordinationDelegateModel cdModel = new CoordinationDelegateModel();
    	cdModel.setName(coordinationDelegateGeneratorRequest.getName());
    	cdModel.setProjectedParticipant(coordinationDelegateGeneratorRequest.getProjectedParticipant());
    	cdModel.setProjectedBpmn2Content(coordinationDelegateGeneratorRequest.getProjectedBpmn2Content());
    	cdModel.setProjectedTypesContent(coordinationDelegateGeneratorRequest.getProjectedTypesContent());
    	
    	cdModel.setWsdlParticipants(coordinationDelegateGeneratorRequest.getWsdlParticipants());
    	cdModel.setCorrelations(coordinationDelegateGeneratorRequest.getCorrelations());

    	Bpmn2ChoreographyModel chorModel = new Bpmn2ChoreographyModel();
    	chorModel.setName(coordinationDelegateGeneratorRequest.getChoreographyName());
    	cdModel.setChoreographyModel(chorModel);

    	CoordinationDelegateModel generatedCdModel = service.generateCoordinationDelegateClient(cdModel);
    	
		String location = synthesisProcessorUtil.createArtifactURI(
				generatedCdModel.getChoreographyModel().getName(), SynthesisProcessorComponentType.CD,
				generatedCdModel.getCdName(), SynthesisProcessorUtil.getTarGzExtension(), generatedCdModel.getArtifact());

		generatedCdModel.setLocation(location);
    	
		return new CoordinationDelegateGeneratorResponse(generatedCdModel.getCdName(), generatedCdModel.getLocation(), null, null);

    }
    
    @RequestMapping(value = "/generateCoordinationDelegateConsumer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public CoordinationDelegateGeneratorResponse generateCoordinationDelegateConsumer(@RequestBody CoordinationDelegateGeneratorRequest coordinationDelegateGeneratorRequest) throws CoordinationDelegateGeneratorException {
	
    	CoordinationDelegateModel cdModel = new CoordinationDelegateModel();
    	cdModel.setName(coordinationDelegateGeneratorRequest.getName());
    	cdModel.setConsumerWsdlContent(coordinationDelegateGeneratorRequest.getConsumerWsdlContent());
    	
    	CoordinationDelegateModel generatedCdModel = service.generateCoordinationDelegateConsumer(cdModel);

		return new CoordinationDelegateGeneratorResponse(generatedCdModel.getName(), null, null, generatedCdModel.getConsumerArtifact());
    	
    }
    
    
}
