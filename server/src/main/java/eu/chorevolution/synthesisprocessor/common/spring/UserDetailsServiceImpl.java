package eu.chorevolution.synthesisprocessor.common.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import eu.chorevolution.synthesisprocessor.business.BusinessException;
import eu.chorevolution.synthesisprocessor.business.UtenteService;
import eu.chorevolution.synthesisprocessor.domain.Utente;

public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UtenteService service;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Utente user;
		try {
			user = service.autentica(username);
		} catch (BusinessException e) {
			throw new UsernameNotFoundException("utente non trovato");
		}

		if (user == null) {
			throw new UsernameNotFoundException("utente non trovato");
		}
		return new UserDetailsImpl(user);

	}

}
