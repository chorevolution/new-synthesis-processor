package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorResponse;
import eu.chorevolution.synthesisprocessor.business.ChoreographyValidator;
import eu.chorevolution.synthesisprocessor.business.model.Bpmn2ChoreographyModel;

@RestController
@RequestMapping("/rest")
public class ChoreographyValidatorController {
	
	@Autowired
	private ChoreographyValidator service;
	
    @RequestMapping(value = "/validateChoreography", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ChoreographyValidatorResponse validateChoreography(@RequestBody ChoreographyValidatorRequest choreographyValidatorRequest) throws ChoreographyValidatorException {
    	
    	Bpmn2ChoreographyModel chorModel = new Bpmn2ChoreographyModel();
    	chorModel.setBpmn2Content(choreographyValidatorRequest.getBpmn2Content());
    	chorModel.setBpmn2XSD(choreographyValidatorRequest.getTypesContent());
    	chorModel.setName(choreographyValidatorRequest.getChoreographyName());
    	
    	Bpmn2ChoreographyModel generatedChorModel = service.validateChoreography(chorModel);

    	return new ChoreographyValidatorResponse(generatedChorModel.isValid(), generatedChorModel.getListErrors());

    }
    
}
