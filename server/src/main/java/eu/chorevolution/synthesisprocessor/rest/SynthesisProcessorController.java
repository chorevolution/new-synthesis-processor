package eu.chorevolution.synthesisprocessor.rest;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.ByteArrayStringType;
import eu.chorevolution.synthesisprocessor.api.domain.SynthesisProcessorComponentType;
import eu.chorevolution.synthesisprocessor.util.SynthesisProcessorUtil;

@RestController
@RequestMapping("/rest")
public class SynthesisProcessorController {
	
	@Value("#{cfg.ARTIFACT_HOME_REPOSITORY}")
	private String artifactHomeRepository;
	
	@Autowired
	private SynthesisProcessorUtil synthesisProcessorUtil;
	
    @RequestMapping(value = "/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public byte[] download(@RequestBody ByteArrayStringType request) throws SynthesisProcessorBusinessException {
    	

		try {

			File artifact = new File(artifactHomeRepository + File.separator + request.getChoreographyName()
					+ File.separator + request.getArtifactType() + File.separator + request.getArtifactName());
			
			if (!artifact.exists()) {
				throw new SynthesisProcessorBusinessException("artifact not found : " + request);
			}

			return FileUtils.readFileToByteArray(artifact);
		} catch (Exception e) {
			throw new SynthesisProcessorBusinessException("artifact not found : " + request, e);
		}

    }
    
    
    @RequestMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String upload(@RequestBody ByteArrayStringType request) throws SynthesisProcessorBusinessException {
    	

		try {
			String name= request.getArtifactName().replace("."+FilenameUtils.getExtension(request.getArtifactName()), "");
			String extenstion = FilenameUtils.getExtension(request.getArtifactName());
			byte[] content = request.getByteArray();

			return synthesisProcessorUtil.createArtifactURI(
					request.getChoreographyName(), SynthesisProcessorComponentType.PROSUMER,
					name, "."+extenstion, content);
		} catch (Exception e) {
			throw new SynthesisProcessorBusinessException("error upload artifact : " + request.getArtifactName(), e);
		}

    }
    
}
