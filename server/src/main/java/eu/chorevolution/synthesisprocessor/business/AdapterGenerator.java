package eu.chorevolution.synthesisprocessor.business;

import eu.chorevolution.synthesisprocessor.api.AdapterGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.AdapterGeneratorResponse;
import eu.chorevolution.synthesisprocessor.business.model.AdapterModel;

public interface AdapterGenerator {

	AdapterModel generateAdapter(AdapterModel adapterModel) throws AdapterGeneratorException;

	
	
}
