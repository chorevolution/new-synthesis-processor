package eu.chorevolution.synthesisprocessor.business.impl;

import org.springframework.stereotype.Service;

import eu.chorevolution.synthesisprocessor.api.BindingComponentGeneratorException;
import eu.chorevolution.synthesisprocessor.business.BindingComponentGenerator;
import eu.chorevolution.synthesisprocessor.business.model.BindingComponentModel;
import eu.chorevolution.transformations.generativeapproach.bcgenerator.BCGenerator;
import eu.chorevolution.transformations.generativeapproach.bcgenerator.BCProtocolType;
import eu.chorevolution.transformations.generativeapproach.bcgenerator.impl.BCGeneratorImpl;
import eu.chorevolution.transformations.generativeapproach.bcgenerator.model.BC;

@Service
public class BindingComponentGeneratorImpl implements BindingComponentGenerator {

	
	@Override
	public BindingComponentModel generateBindingComponent(BindingComponentModel bcModel) throws BindingComponentGeneratorException {
		
		try {
			BCProtocolType protocolType;
			switch (bcModel.getBindingComponentProtocolType()) {
			case SOAP:
				protocolType = BCProtocolType.SOAP;
				break;
			case REST:
				protocolType = BCProtocolType.REST;
				break;
			default:
				protocolType = BCProtocolType.SOAP;
				break;
			}


			BCGenerator bcGenerator = new BCGeneratorImpl();
			
			BC response = bcGenerator.generateBC(bcModel.getBindingComponentName(), bcModel.getInterfaceDescriptionContent(), protocolType);

			bcModel.setArtifact(response.getArtifact());
			bcModel.setBcWsdl(response.getWsdl());
			
			return bcModel;

		} catch (Exception e) {
			e.printStackTrace();
			throw new BindingComponentGeneratorException(e);
		}
		
		
	}

	
	
}
