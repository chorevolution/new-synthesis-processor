package eu.chorevolution.synthesisprocessor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.chorevolution.synthesisprocessor.api.ChoreographyProjectionGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyProjectionGeneratorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyProjectionGeneratorResponse;
import eu.chorevolution.synthesisprocessor.business.ChoreographyProjectionGenerator;
import eu.chorevolution.synthesisprocessor.business.model.ChoreographyProjectionModel;

@RestController
@RequestMapping("/rest")
public class ChoreographyProjectionController {
	
	@Autowired
	private ChoreographyProjectionGenerator service;
	
    @RequestMapping(value = "/generateChoreographyProjection", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ChoreographyProjectionGeneratorResponse generateChoreographyProjection(@RequestBody ChoreographyProjectionGeneratorRequest choreographyProjectionGeneratorRequest) throws ChoreographyProjectionGeneratorException {
    	
    	ChoreographyProjectionModel cpModel = new ChoreographyProjectionModel();
    	cpModel.setBpmn2Content(choreographyProjectionGeneratorRequest.getBpmn2Content());
    	cpModel.setParticipant(choreographyProjectionGeneratorRequest.getParticipant());
    	
    	ChoreographyProjectionModel generatedCpModel = service.generateChoreographyProjection(cpModel);

		return new ChoreographyProjectionGeneratorResponse(generatedCpModel.getProjectedBpmn2Content());

    	
    }
    
}
