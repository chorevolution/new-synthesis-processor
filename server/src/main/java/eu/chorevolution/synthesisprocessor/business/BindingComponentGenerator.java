package eu.chorevolution.synthesisprocessor.business;

import eu.chorevolution.synthesisprocessor.api.BindingComponentGeneratorException;
import eu.chorevolution.synthesisprocessor.api.domain.BindingComponentGeneratorResponse;
import eu.chorevolution.synthesisprocessor.business.model.BindingComponentModel;

public interface BindingComponentGenerator {

	BindingComponentModel generateBindingComponent(BindingComponentModel bcModel)
			throws BindingComponentGeneratorException;

}
