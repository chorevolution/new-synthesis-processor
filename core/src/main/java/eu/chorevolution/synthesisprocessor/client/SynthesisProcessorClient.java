/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package eu.chorevolution.synthesisprocessor.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.synthesisprocessor.api.ChoreographyValidatorException;
import eu.chorevolution.synthesisprocessor.api.SynthesisProcessorBusinessException;
import eu.chorevolution.synthesisprocessor.api.domain.ByteArrayStringType;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorRequest;
import eu.chorevolution.synthesisprocessor.api.domain.ChoreographyValidatorResponse;
import eu.chorevolution.synthesisprocessor.util.ClientPathUtils;
import eu.chorevolution.synthesisprocessor.util.GeneralUtils;

public class SynthesisProcessorClient {

	private RestTemplate restTemplate = new RestTemplate();
	private String host;
	
	public SynthesisProcessorClient(String host) {
		this.host = host;
	}
	
	public byte[] download(String choreographyName, String artifactType, String artifactName)
			throws SynthesisProcessorBusinessException {
		
        HttpEntity<ByteArrayStringType> entity = new HttpEntity<ByteArrayStringType>(new ByteArrayStringType(
        		null, choreographyName, artifactType, artifactName), GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<byte[]> result = restTemplate.exchange(host + ClientPathUtils.DOWNLOAD_PATH, 
            		HttpMethod.POST, entity, byte[].class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new SynthesisProcessorBusinessException("", e);
		}
		
		/*
		Map<String, String> params = new HashMap<String, String>();
		params.put("request", choreographyName+File.separator+artifactType+File.separator+artifactName);
		
		try {
			return restTemplate.getForObject(host + ClientPathUtils.DOWNLOAD_PATH, byte[].class, params);
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new SynthesisProcessorBusinessException("", e);
		}
		*/
		
	}
	
	public String upload(String choreographyName, String artifactType, String artifactName, InputStream artifactContent)
			throws SynthesisProcessorBusinessException, IOException {
		
		
        HttpEntity<ByteArrayStringType> entity = new HttpEntity<ByteArrayStringType>(new ByteArrayStringType(
        		IOUtils.toByteArray(artifactContent), choreographyName, artifactType, artifactName), GeneralUtils.getDefaultHttpHeaders());
		
		try {
            ResponseEntity<String> result = restTemplate.exchange(host + ClientPathUtils.UPLOAD_PATH, 
            		HttpMethod.POST, entity, String.class);
			return result.getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new SynthesisProcessorBusinessException("", e);
		}
		
		/*
		WebClient client = super.setupClient();
		client.path(choreographyName).path(artifactType).path(artifactName);
		try {
			return client.post(artifactContent, String.class);
		} catch (WebApplicationException e) {
			throw new SynthesisProcessorBusinessException(e);
		}
		

		return null;
		*/
	}



	

}
