<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="login-box-body">
	<p class="login-box-msg"><spring:message code="common.loginheader"/></p>
	<c:if test="${param.error != null}">
		<p><spring:message code="common.loginerror"/></p>
	</c:if>
	<form action="${pageContext.request.contextPath}/login" method="post">
	  <div class="form-group has-feedback">
	    <input type="text" class="form-control" placeholder="Email" name="username">
	    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	  </div>
	  <div class="form-group has-feedback">
	    <input type="password" class="form-control" placeholder="Password" name="password">
	    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	  </div>
	  <div class="row">
	    <div class="col-xs-8">
	    </div>
	    <div class="col-xs-4">
	      <button type="submit" class="btn btn-primary btn-block btn-flat"><spring:message code="common.button.loginenter"/></button>
	    </div>
	  </div>
	</form>
</div>