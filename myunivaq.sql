CREATE TABLE `utenti` (
  `id_utente` INT(10) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `password` VARBINARY(40) NOT NULL,
  `nome` VARCHAR(50) NOT NULL,
  `cognome` VARCHAR(50) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `matricola` VARCHAR(100) NOT NULL,
  `data_nascita` date NOT NULL,  
  `tipologia` int(10) NOT NULL,
  PRIMARY KEY (`id_utente`));

CREATE TABLE `ruoli` (
  `id_ruolo` INT(10) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(20) NOT NULL,
  `descrizione` VARCHAR(100) NULL,
  PRIMARY KEY (`id_ruolo`));

CREATE TABLE `aree_ssd` (
  `id_area_ssd` INT(10) NOT NULL AUTO_INCREMENT,
  `codice` VARCHAR(20) NOT NULL,
  `denominazione` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id_area_ssd`));
  
CREATE TABLE `settori_scientifico_disciplinari` (
  `id_ssd` INT(10) NOT NULL AUTO_INCREMENT,
  `codice` VARCHAR(20) NOT NULL,
  `denominazione` VARCHAR(200) NOT NULL,
  `id_area_ssd` INT(10) NOT NULL,
  PRIMARY KEY (`id_ssd`),
  CONSTRAINT `fk_aree_ssd_id_area_ssd`
    FOREIGN KEY (`id_area_ssd`)
    REFERENCES `aree_ssd` (`id_area_ssd`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);  
  
CREATE TABLE `ruoli_utenti` (
  `id_utente` INT(10) NOT NULL,
  `id_ruolo` INT(10) NOT NULL,
  INDEX `fk_ruoli_utenti_id_utente_idx` (`id_utente` ASC),
  INDEX `fk_ruoli_utenti_id_ruolo_idx` (`id_ruolo` ASC),
  CONSTRAINT `fk_ruoli_utenti_id_utente`
    FOREIGN KEY (`id_utente`)
    REFERENCES `utenti` (`id_utente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ruoli_utenti_id_ruolo`
    FOREIGN KEY (`id_ruolo`)
    REFERENCES `ruoli` (`id_ruolo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
INSERT INTO `ruoli` (`id_ruolo`, `nome`) VALUES (1, 'amministratore'), (2, 'presidentecad'), (3, 'segreteriadidattica'), (4, 'studente');
INSERT INTO `utenti` (`id_utente`, `username`, `password`, `nome`, `cognome`, `email`, `matricola`, `data_nascita`, `tipologia`) VALUES (1, 'admin', 'admin', 'Amleto', 'Di Salle', 'amleto.disalle@univaq.it', '2996', '1971-10-09', 1);
INSERT INTO `utenti` (`id_utente`, `username`, `password`, `nome`, `cognome`, `email`, `matricola`, `data_nascita`, `tipologia`) VALUES (2, 'presidentecad', 'presidentecad', 'Amleto', 'Di Salle', 'amleto.disalle@univaq.it', '2996', '1971-10-09', 1);
INSERT INTO `utenti` (`id_utente`, `username`, `password`, `nome`, `cognome`, `email`, `matricola`, `data_nascita`, `tipologia`) VALUES (3, 'segreteriadidattica', 'segreteriadidattica', 'Amleto', 'Di Salle', 'amleto.disalle@univaq.it', '2996', '1971-10-09', 1);
INSERT INTO `utenti` (`id_utente`, `username`, `password`, `nome`, `cognome`, `email`, `matricola`, `data_nascita`, `tipologia`) VALUES (4, 'studente', 'studente', 'Amleto', 'Di Salle', 'amleto.disalle@univaq.it', '2996', '1971-10-09', 1);

INSERT INTO `ruoli_utenti` (`id_utente`, `id_ruolo`) VALUES (1, 1), (2, 2), (3, 3), (4, 4);

INSERT INTO `aree_ssd` (`id_area_ssd`, `codice`, `denominazione`) VALUES
(1, '01', 'Scienze matematiche e informatiche'),
(2, '02', 'Scienze fisiche'),
(3, '03', 'Scienze chimiche'),
(4, '04', 'Scienze della terra'),
(5, '05', 'Scienze biologiche'),
(6, '06', 'Scienze mediche'),
(7, '07', 'Scienze agrarie e veterinarie'),
(8, '08', 'Ingegneria civile e Architettura'),
(9, '09', 'Ingegneria industriale e dell''informazione'),
(10, '10', 'Scienze dell''antichità, filologico-letterarie e storico-artistiche'),
(11, '11', 'Scienze storiche, filosofiche, pedagogiche e psicologiche'),
(12, '12', 'Scienze giuridiche'),
(13, '13', 'Scienze economiche e statistiche'),
(14, '14', 'Scienze politiche e sociali');

INSERT INTO `settori_scientifico_disciplinari` (`id_ssd`, `codice`, `denominazione`, `id_area_ssd`) VALUES
(1, 'MAT/01', 'LOGICA MATEMATICA', 1),
(2, 'MAT/02', 'ALGEBRA', 1),
(3, 'INF/01', 'INFORMATICA ', 1);
