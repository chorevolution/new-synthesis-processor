package eu.chorevolution.synthesisprocessor.business;

import eu.chorevolution.synthesisprocessor.domain.Utente;

public interface UtenteService {

	Utente autentica(String username) throws BusinessException;

}
